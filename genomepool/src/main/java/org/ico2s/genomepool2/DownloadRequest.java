/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.ico2s.genomepool2;

/**
 * @author Keith Flanagan
 */
public class DownloadRequest {

  private String sourceUrl;

  private String successTopic;
  private String failureTopic;

  public DownloadRequest() {
  }

  public DownloadRequest(String sourceUrl, String successTopic, String failureTopic) {
    this.sourceUrl = sourceUrl;
    this.successTopic = successTopic;
    this.failureTopic = failureTopic;
  }

  public String getSourceUrl() {
    return sourceUrl;
  }

  public void setSourceUrl(String sourceUrl) {
    this.sourceUrl = sourceUrl;
  }

  public String getSuccessTopic() {
    return successTopic;
  }

  public void setSuccessTopic(String successTopic) {
    this.successTopic = successTopic;
  }

  public String getFailureTopic() {
    return failureTopic;
  }

  public void setFailureTopic(String failureTopic) {
    this.failureTopic = failureTopic;
  }
}
