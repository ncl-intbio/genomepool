/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool2.parsers.assembly_summary;

import java.io.Serializable;
import java.util.Date;

/**
 * Represents a single entry in an NCBI "assembly_summary.txt" file.
 *
 * See NCBI documentation for detailed field info:
 * http://www.ncbi.nlm.nih.gov/assembly/help/
 * ftp://ftp.ncbi.nlm.nih.gov/genomes/README_assembly_summary.txt
 *
 * @author Keith Flanagan
 */
public class AssemblySummary implements Serializable {
  public enum RefseqCategory {
    /**
     * A manually selected high quality genome assembly that NCBI and the community have
     * identified as being important as a standard against which other data are compared.
     */
    REFERENCE_GENOME ("reference genome"),
    /**
     * A genome computationally or manually selected as a representative from among the best
     * genomes available for a species or clade that does not have a designated reference genome.
     */
    REPRESENTATIVE_GENOME("representative genome"),

    /**
     * No RefSeq category assigned to this assembly.
     */
    NA ("na");

    RefseqCategory(String ncbiName) {
      this.ncbiName = ncbiName;
    }

    private final String ncbiName;

    public String getNcbiName() {
      return ncbiName;
    }
  }

  public enum VersionStatus {
    LATEST ("latest"),
    REPLACED ("replaced");

    VersionStatus(String ncbiName) {
      this.ncbiName = ncbiName;
    }

    private final String ncbiName;

    public String getNcbiName() {
      return ncbiName;
    }
  }

  public enum AssemblyLevel {
    CONTIG ("Contig"),
    SCAFFOLD ("Scaffold"),
    COMPLETE_GENOME ("Complete Genome"),
    CHROMOSOME ("Chromosome");

    AssemblyLevel(String ncbiName) {
      this.ncbiName = ncbiName;
    }

    private final String ncbiName;

    public String getNcbiName() {
      return ncbiName;
    }
  }

  public enum ReleaseType {
    MAJOR ("Major"),
    MINOR ("Minor"),
    PATCH ("Patch");

    ReleaseType(String ncbiName) {
      this.ncbiName = ncbiName;
    }

    private final String ncbiName;

    public String getNcbiName() {
      return ncbiName;
    }
  }

  public enum GenomeRepresentation {
    FULL ("Full"),
    PARTIAL ("Partial");

    GenomeRepresentation(String ncbiName) {
      this.ncbiName = ncbiName;
    }

    private final String ncbiName;

    public String getNcbiName() {
      return ncbiName;
    }
  }

  public enum PairedAsmComp {
    IDENTICAL ("identical"), // GenBank and RefSeq assemblies are identical
    DIFFERENT ("different"), // GenBank and RefSeq assemblies are not identical
    NA ("na");               // not applicable since the assembly is unpaired

    PairedAsmComp(String ncbiName) {
      this.ncbiName = ncbiName;
    }

    private final String ncbiName;

    public String getNcbiName() {
      return ncbiName;
    }
  }

  private String assemblyAccession;       // [required] GCF_000154625.1
  private String bioProjectId;            // [required] PRJNA54949
  private String bioSampleId;             // [required] SAMN02470541
  private String wgsMaster;               // [optional] (‘’ | ABHD00000000.2)
  //  RefSeq category - shown if the assembly is a RefSeq reference genome or RefSeq representative genome.
  private RefseqCategory refseqCategory;  // [optional] (na | reference_genome | representative genome )
  private String taxId;                   // [required] 479831
  private String speciesTaxId;            // [required] 1496
  private String organismName;            // [required] Peptoclostridium difficile QCD-63q42
  private String infraSpecificName;       // [optional] (‘’ | strain=630)
  private String isolate;                 // [optional] (‘’ | QCD-63q42)
  private VersionStatus versionStatus;    // [required] (latest | replaced)
  private AssemblyLevel assemblyLevel;    // [required] (Complete Genome | Chromosome | Scaffold | Contig)
  private ReleaseType releaseType;        // [required] (Major | Minor | Patch)
  // Genome representation - whether the goal for the assembly was to represent the whole genome or only part of it:
  private GenomeRepresentation genomeRep; // [required] (Full | Partial)
  private Date seqRelDate;                // [required] 2009/01/08
  private String asmName;                 // [required] ASM15462v1
  private String submitter;               // [optional] (‘’ | McGill University)
  private String gbrsPairedAsm;           // [required] (GCA_000154625.1)
  //Paired assembly comparison: whether the paired GenBank & RefSeq assemblies are identical or different.
  private PairedAsmComp pairedAsmComp;    // [required] (identical | different | na)
  private String ftpPath;                 // [required] (ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF_000154625.1_ASM15462v1)
  private String assemblyDirectoryName;   // For convenience, contains the last directory from ftpPath. e.g.: 'GCF_000154625.1_ASM15462v1'

  public AssemblySummary() {
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    AssemblySummary that = (AssemblySummary) o;

    if (!assemblyAccession.equals(that.assemblyAccession)) return false;
    return asmName.equals(that.asmName);

  }

  @Override
  public int hashCode() {
    int result = assemblyAccession.hashCode();
    result = 31 * result + asmName.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "AssemblySummary{" +
        "assemblyAccession='" + assemblyAccession + '\'' +
        ", bioProjectId='" + bioProjectId + '\'' +
        ", bioSampleId='" + bioSampleId + '\'' +
        ", wgsMaster='" + wgsMaster + '\'' +
        ", refseqCategory=" + refseqCategory +
        ", taxId='" + taxId + '\'' +
        ", speciesTaxId='" + speciesTaxId + '\'' +
        ", organismName='" + organismName + '\'' +
        ", infraSpecificName='" + infraSpecificName + '\'' +
        ", isolate='" + isolate + '\'' +
        ", versionStatus=" + versionStatus +
        ", assemblyLevel=" + assemblyLevel +
        ", releaseType=" + releaseType +
        ", genomeRep=" + genomeRep +
        ", seqRelDate=" + seqRelDate +
        ", asmName='" + asmName + '\'' +
        ", submitter='" + submitter + '\'' +
        ", gbrsPairedAsm='" + gbrsPairedAsm + '\'' +
        ", pairedAsmComp=" + pairedAsmComp +
        ", ftpPath='" + ftpPath + '\'' +
        ", assemblyDirectoryName='" + assemblyDirectoryName + '\'' +
        '}';
  }

  public String getAssemblyAccession() {
    return assemblyAccession;
  }

  public void setAssemblyAccession(String assemblyAccession) {
    this.assemblyAccession = assemblyAccession;
  }

  public String getBioProjectId() {
    return bioProjectId;
  }

  public void setBioProjectId(String bioProjectId) {
    this.bioProjectId = bioProjectId;
  }

  public String getBioSampleId() {
    return bioSampleId;
  }

  public void setBioSampleId(String bioSampleId) {
    this.bioSampleId = bioSampleId;
  }

  public String getWgsMaster() {
    return wgsMaster;
  }

  public void setWgsMaster(String wgsMaster) {
    this.wgsMaster = wgsMaster;
  }

  public RefseqCategory getRefseqCategory() {
    return refseqCategory;
  }

  public void setRefseqCategory(RefseqCategory refseqCategory) {
    this.refseqCategory = refseqCategory;
  }

  public String getTaxId() {
    return taxId;
  }

  public void setTaxId(String taxId) {
    this.taxId = taxId;
  }

  public String getSpeciesTaxId() {
    return speciesTaxId;
  }

  public void setSpeciesTaxId(String speciesTaxId) {
    this.speciesTaxId = speciesTaxId;
  }

  public String getOrganismName() {
    return organismName;
  }

  public void setOrganismName(String organismName) {
    this.organismName = organismName;
  }

  public String getInfraSpecificName() {
    return infraSpecificName;
  }

  public void setInfraSpecificName(String infraSpecificName) {
    this.infraSpecificName = infraSpecificName;
  }

  public String getIsolate() {
    return isolate;
  }

  public void setIsolate(String isolate) {
    this.isolate = isolate;
  }

  public VersionStatus getVersionStatus() {
    return versionStatus;
  }

  public void setVersionStatus(VersionStatus versionStatus) {
    this.versionStatus = versionStatus;
  }

  public AssemblyLevel getAssemblyLevel() {
    return assemblyLevel;
  }

  public void setAssemblyLevel(AssemblyLevel assemblyLevel) {
    this.assemblyLevel = assemblyLevel;
  }

  public ReleaseType getReleaseType() {
    return releaseType;
  }

  public void setReleaseType(ReleaseType releaseType) {
    this.releaseType = releaseType;
  }

  public GenomeRepresentation getGenomeRep() {
    return genomeRep;
  }

  public void setGenomeRep(GenomeRepresentation genomeRep) {
    this.genomeRep = genomeRep;
  }

  public Date getSeqRelDate() {
    return seqRelDate;
  }

  public void setSeqRelDate(Date seqRelDate) {
    this.seqRelDate = seqRelDate;
  }

  public String getAsmName() {
    return asmName;
  }

  public void setAsmName(String asmName) {
    this.asmName = asmName;
  }

  public String getSubmitter() {
    return submitter;
  }

  public void setSubmitter(String submitter) {
    this.submitter = submitter;
  }

  public String getGbrsPairedAsm() {
    return gbrsPairedAsm;
  }

  public void setGbrsPairedAsm(String gbrsPairedAsm) {
    this.gbrsPairedAsm = gbrsPairedAsm;
  }

  public PairedAsmComp getPairedAsmComp() {
    return pairedAsmComp;
  }

  public void setPairedAsmComp(PairedAsmComp pairedAsmComp) {
    this.pairedAsmComp = pairedAsmComp;
  }

  public String getFtpPath() {
    return ftpPath;
  }

  public void setFtpPath(String ftpPath) {
    this.ftpPath = ftpPath;
  }

  public String getAssemblyDirectoryName() {
    return assemblyDirectoryName;
  }

  public void setAssemblyDirectoryName(String assemblyDirectoryName) {
    this.assemblyDirectoryName = assemblyDirectoryName;
  }
}
