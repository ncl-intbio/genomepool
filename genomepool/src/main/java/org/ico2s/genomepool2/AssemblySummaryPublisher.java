/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.ico2s.genomepool2;

import com.google.gson.Gson;
import com.microbasecloud.halogen.HalAuth;
import com.microbasecloud.halogen.Message;
import com.microbasecloud.halogen.MessageFactory;
import com.microbasecloud.halogen.publisher.HalPublisher;
import com.microbasecloud.halogen.publisher.HalPublisherWsClient;
import com.microbasecloud.halogen.wrappers.HalPublisherWrapper;
import com.microbasecloud.util.GsonFactory;
import org.apache.commons.cli.*;
import org.ico2s.genomepool2.parsers.assembly_summary.AssemblySummary;
import org.ico2s.genomepool2.parsers.assembly_summary.AssemblySummaryParser;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import static org.apache.commons.cli.OptionBuilder.withLongOpt;

/**
 * Given an assembly_summary.txt file (e.g., ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria), publishes a download
 * job description for each entry.
 *
 *
 * @author Keith Flanagan
 */
public class AssemblySummaryPublisher {
  private static final String DEFAULT_CHANNEL = "isense";
  private static final String DEFAULT_TOPIC = "DownloadRequest";

  private static final String DEFAULT_FNA_SUCCESS_TOPIC = "DnaDownloaded";
  private static final String DEFAULT_FAA_SUCCESS_TOPIC = "ProteinsDownloaded";
  private static final String DEFAULT_GBK_SUCCESS_TOPIC = "GbkDownloaded";
  private static final String DEFAULT_FAILURE_TOPIC = "DownloadFailed";

  private static void printHelpExit(Options options) {
    HelpFormatter formatter = new HelpFormatter();
    String cmdSyntax = "AssemblySummaryPublisher.sh";
    String header = "";
    String footer = "";
    int width = 80;
    formatter.printHelp(width, cmdSyntax, header, options, footer);
    System.exit(0);
  }

  public static void main(String[] args) throws Exception {
    CommandLineParser clParser = new DefaultParser();

    Options options = new Options();

    options.addOption(withLongOpt("assemblies").hasArgs().withDescription(
        "The URL of an assembly_summary.txt file. Examples: ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/assembly_summary.txt or file://foo/bar/assembly_summary.txt").create());

    options.addOption(withLongOpt("halogen-url").hasArgs().withDescription(
        "The location of a Halogen publisher").create());

    options.addOption(withLongOpt("publisher").hasArgs().withDescription(
        "The halogen publisher key pair to use.").create());

    options.addOption(withLongOpt("channel").hasArgs().withDescription(
        "[OPTIONAL] Overrides the default publisher channel").create());

    options.addOption(withLongOpt("topic").hasArgs().withDescription(
        "[OPTIONAL] Overrides the default publisher topic").create());

    if (args.length == 0) {
      printHelpExit(options);
    }

    URL assembliesUrl;
    String halogenUrl;
    HalAuth publisherAuth;

    String channel = DEFAULT_CHANNEL;
    String topic = DEFAULT_TOPIC;

    try {
      CommandLine line = clParser.parse(options, args);
      if (line.hasOption("assemblies")) {
        assembliesUrl = new URL(line.getOptionValue("assemblies"));
      } else {
        throw new RuntimeException("You forgot to specify a URL to an NCBI assembly_summary.txt file. Examples: " +
            "ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/assembly_summary.txt or file://foo/bar/assembly_summary.txt");
      }

      if (line.hasOption("halogen-url")) {
        halogenUrl = line.getOptionValue("halogen-url");
      } else {
        throw new RuntimeException("You forgot to specify a local storage directory");
      }

      if (line.hasOption("publisher")) {
        String[] parsed = line.getOptionValues("publisher");
        publisherAuth = new HalAuth(UUID.fromString(parsed[0]), UUID.fromString(parsed[1]));
      } else {
        throw new RuntimeException("A publisher key pair must be provided! eg:"+UUID.randomUUID());
      }

      if (line.hasOption("channel")) {
        channel = line.getOptionValue("channel");
      }
      if (line.hasOption("topic")) {
        channel = line.getOptionValue("topic");
      }

    } catch (Exception e) {
      e.printStackTrace();
      printHelpExit(options);
      throw new RuntimeException("Failed!", e);
    }


    Set<AssemblySummary> filteredAssemblies = new HashSet<>();
    try (InputStream is = assembliesUrl.openStream()) {
      AssemblySummaryParser parser = new AssemblySummaryParser();
      Set<AssemblySummary> assemblies = parser.parse(is);

      System.out.println(String.format("Parsed %s assemblies from file %s", assemblies.size(), assembliesUrl.toString()));

      for (AssemblySummary assembly : assemblies) {
        if (assembly.getVersionStatus() != AssemblySummary.VersionStatus.LATEST) {
          continue; // Ignore old entries
        }

        // Skip entries that didn't intend to sequence the full genome
        if (assembly.getGenomeRep() != AssemblySummary.GenomeRepresentation.FULL) {
          continue;
        }

        // Skip entries that aren't annotated as a complete genome sequence
        if (assembly.getAssemblyLevel() != AssemblySummary.AssemblyLevel.COMPLETE_GENOME) {
          continue;
        }

        filteredAssemblies.add(assembly);
      }

    }
    catch (Exception e) {
      throw new Exception("Failed to parse assemblies!", e);
    }

    System.out.println(String.format("After filtering, %s assemblies remained", filteredAssemblies.size()));

    Gson gson = GsonFactory.make();

    HalPublisher publisher = new HalPublisherWrapper(gson, new HalPublisherWsClient(gson, halogenUrl, publisherAuth));
    MessageFactory messageFactory = new MessageFactory(publisherAuth.getId(), channel, topic);

    List<Message<DownloadRequest>> messages = new ArrayList<>(64);
    for (AssemblySummary assembly : filteredAssemblies) {
      messages.add(messageFactory.make(new DownloadRequest(urlForAssemblyGenbank(assembly).toString(), DEFAULT_GBK_SUCCESS_TOPIC, DEFAULT_FAILURE_TOPIC)));
      messages.add(messageFactory.make(new DownloadRequest(urlForAssemblyDna(assembly).toString(), DEFAULT_FNA_SUCCESS_TOPIC, DEFAULT_FAILURE_TOPIC)));
      messages.add(messageFactory.make(new DownloadRequest(urlForAssemblyProtein(assembly).toString(), DEFAULT_FAA_SUCCESS_TOPIC, DEFAULT_FAILURE_TOPIC)));
    }

    System.out.println(String.format("Publishing %s messages", messages.size()));

    publisher.publishAll(messages);
  }

  private static URL urlForAssemblyDna(AssemblySummary assemblySummary) throws MalformedURLException {
    /*
     * The AssemblySummary only provides an FTP directory. We need to create the filename from other metadata.
     */
    String urlText = assemblySummary.getFtpPath() + "/" +
        assemblySummary.getAssemblyDirectoryName() +
        "_genomic.faa.gz";

    return new URL(urlText);
  }

  private static URL urlForAssemblyProtein(AssemblySummary assemblySummary) throws MalformedURLException {
    /*
     * The AssemblySummary only provides an FTP directory. We need to create the filename from other metadata.
     */
    String urlText = assemblySummary.getFtpPath() + "/" +
        assemblySummary.getAssemblyDirectoryName() +
        "_protein.faa.gz";

    return new URL(urlText);
  }

  private static URL urlForAssemblyGenbank(AssemblySummary assemblySummary) throws MalformedURLException {
    /*
     * The AssemblySummary only provides an FTP directory. We need to create the filename from other metadata.
     */
    String urlText = assemblySummary.getFtpPath() + "/" +
        assemblySummary.getAssemblyDirectoryName() +
        "_genomic.gbff.gz";

    return new URL(urlText);
  }
}
