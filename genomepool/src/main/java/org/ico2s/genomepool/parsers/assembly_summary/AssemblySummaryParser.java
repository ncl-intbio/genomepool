/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.parsers.assembly_summary;

import org.ico2s.genomepool.parsers.genbank.ParserException;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Parses an <code>assembly_summary.txt</code>-style file.
 *
 * For example:
 * ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/Bacillus_subtilis/assembly_summary.txt
 *
 * @author Keith Flanagan
 */
public class AssemblySummaryParser {
  public static final SimpleDateFormat dateParser = new SimpleDateFormat("yyyy/MM/dd");

  public Set<AssemblySummary> parse(InputStream is) throws ParserException {
    try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
      Set<AssemblySummary> parsed = new HashSet<>(32);
      for (String line = br.readLine(); line != null; line = br.readLine()) {
        if (line.startsWith("#")) {
          continue; // Comment - usually the header line
        }
        parsed.add(parseLine(line));
      }
      return parsed;
    } catch (Exception e) {
      throw new ParserException("Failed to parse", e);
    }
  }

  private AssemblySummary parseLine(String line) throws ParserException {
    StringTokenizer st = new StringTokenizer(line, "\t", true);

    AssemblySummary entry = new AssemblySummary();
    try {
      entry.setAssemblyAccession(nextToken(st));
      entry.setBioProjectId(nextToken(st));
      entry.setBioSampleId(nextToken(st));
      entry.setWgsMaster(nextToken(st));

      entry.setRefseqCategory(nextRefseqCategory(st));
      entry.setTaxId(nextToken(st));
      entry.setSpeciesTaxId(nextToken(st));
      entry.setOrganismName(nextToken(st));
      entry.setInfraSpecificName(nextToken(st));
      entry.setIsolate(nextToken(st));
      entry.setVersionStatus(nextVersionStatus(st));
      entry.setAssemblyLevel(nextAssemblyLevel(st));
      entry.setReleaseType(nextReleaseType(st));
      entry.setGenomeRep(nextGenomeRep(st));
      entry.setSeqRelDate(nextDate(st));
      entry.setAsmName(nextToken(st));
      entry.setSubmitter(nextToken(st));
      entry.setGbrsPairedAsm(nextToken(st));
      entry.setPairedAsmComp(nextPairedAsmComp(st));
      entry.setFtpPath(nextToken(st));

      String fullPath = entry.getFtpPath();
      entry.setAssemblyDirectoryName(fullPath.substring(fullPath.lastIndexOf("/")+1));
    } catch (Exception e) {
      throw new ParserException("Failed to parse file. Failing line was: "+line, e);
    }

    return entry;
  }

  private String nextToken(StringTokenizer st) {
    String next = st.nextToken();
    if (next.equals("\t")) {
      return null;  // There was no entry in this field
    }

    if (st.hasMoreTokens()) {
      st.nextToken(); // Remove next delimiter
    }
    return next;
  }

  private AssemblySummary.RefseqCategory nextRefseqCategory(StringTokenizer st) throws ParserException {
    String next = nextToken(st);
    if (next == null) {
      return null;
    }
    for (AssemblySummary.RefseqCategory v : AssemblySummary.RefseqCategory.values()) {
      if (v.getNcbiName().equals(next)) {
        return v;
      }
    }

    throw new ParserException("Unrecognised enum value: "+next);
  }

  private AssemblySummary.VersionStatus nextVersionStatus(StringTokenizer st) throws ParserException {
    String next = nextToken(st);
    if (next == null) {
      return null;
    }
    for (AssemblySummary.VersionStatus v : AssemblySummary.VersionStatus.values()) {
      if (v.getNcbiName().equals(next)) {
        return v;
      }
    }

    throw new ParserException("Unrecognised enum value: "+next);
  }

  private AssemblySummary.AssemblyLevel nextAssemblyLevel(StringTokenizer st) throws ParserException {
    String next = nextToken(st);
    if (next == null) {
      return null;
    }
    for (AssemblySummary.AssemblyLevel v : AssemblySummary.AssemblyLevel.values()) {
      if (v.getNcbiName().equals(next)) {
        return v;
      }
    }

    throw new ParserException("Unrecognised enum value: "+next);
  }

  private AssemblySummary.ReleaseType nextReleaseType(StringTokenizer st) throws ParserException {
    String next = nextToken(st);
    if (next == null) {
      return null;
    }
    for (AssemblySummary.ReleaseType v : AssemblySummary.ReleaseType.values()) {
      if (v.getNcbiName().equals(next)) {
        return v;
      }
    }

    throw new ParserException("Unrecognised enum value: "+next);
  }

  private AssemblySummary.GenomeRepresentation nextGenomeRep(StringTokenizer st) throws ParserException {
    String next = nextToken(st);
    if (next == null) {
      return null;
    }
    for (AssemblySummary.GenomeRepresentation v : AssemblySummary.GenomeRepresentation.values()) {
      if (v.getNcbiName().equals(next)) {
        return v;
      }
    }

    throw new ParserException("Unrecognised enum value: "+next);
  }

  private Date nextDate(StringTokenizer st) throws ParserException {
    String next = nextToken(st);
    try {
      return next == null ? null : dateParser.parse(next);
    } catch (Exception e) {
      throw new ParserException("Failed to parse a date from: "+next);
    }
  }

  private AssemblySummary.PairedAsmComp nextPairedAsmComp(StringTokenizer st)
      throws ParserException {
    String next = nextToken(st);
    if (next == null) {
      return null;
    }
    for (AssemblySummary.PairedAsmComp v : AssemblySummary.PairedAsmComp.values()) {
      if (v.getNcbiName().equals(next)) {
        return v;
      }
    }

    throw new ParserException("Unrecognised enum value: "+next);
  }
}
