/*
 * Copyright 2013 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.parsers.genbank.data;

import java.io.Serializable;
import java.util.List;

/**
 * Example:
 * <pre>
 * LOCUS       NC_018492             218786 bp    DNA     circular CON 11-JUN-2013
 * DEFINITION  Bacillus cereus FRI-35 plasmid p01, complete sequence.
 * ACCESSION   NC_018492
 * VERSION     NC_018492.1  GI:402557992
 * DBLINK      BioProject: PRJNA224116
 *             Assembly: GCF_000497485.1
 *             BioSample: SAMN02641633
 * KEYWORDS    .
 * SOURCE      Bacillus cereus FRI-35
 *   ORGANISM    Bacillus cereus FRI-35
 *               Bacteria; Firmicutes; Bacilli; Bacillales; Bacillaceae; Bacillus;
 *               Bacillus cereus group.
 * REFERENCE   1  (bases 1 to 218786)
 * CONSRTM   NCBI Genome Project
 * TITLE     Direct Submission
 * JOURNAL   Submitted (21-AUG-2012) National Center for Biotechnology
 *           Information, NIH, Bethesda, MD 20894, USA
 * REFERENCE   2  (bases 1 to 218786)
 * AUTHORS   Doggett,N., Lu,M., Bruce,D., Detter,J.C., Johnson,S.L. and Han,C.
 * TITLE     Direct Submission
 * JOURNAL   Submitted (09-AUG-2012) BioScience Division, Los Alamos National
 *           Laboratory, PO Box 1663 M888, Los Alamos, NM 87544, USA
 * COMMENT     PROVISIONAL REFSEQ: This record has not yet been subject to final
 *             NCBI review. The reference sequence is identical to CP003748.
 *             Annotation was added by the NCBI Prokaryotic Genomes Automatic
 *             Annotation Pipeline Group. Information about the Pipeline can be
 *             found here:
 *             http://www.ncbi.nlm.nih.gov/genomes/static/Pipeline.html. Please be
 *             aware that the annotation is done automatically with little or no
 *             manual curation.
 *             Source DNA available from Norman Dogget, BioScience Division, Los
 *             Alamos National Laboratory, PO Box 1663 M888, Los Alamos, NM 87544.
 *             COMPLETENESS: full length.
 * </pre>
 * @author Keith Flanagan
 */
public class GbkHeader implements Serializable {
  private String bioProjectId;
  private String assemblyId;
  private String assemblyName;
  private String bioSampleId;

  private String locus;
  private String definition;
  private List<String> accessions;
  private String version;
  private String organismName;     // The first line of the Genbank ORGANISM entry
  private String organismFullText; // The full multi-line Genbank ORGANISM entry
  private String comment;

  // Data received from the 'source' Feature
  private int length;
  private String taxonId;
  private String subSpeciesName;
  private String strainName;
  private String plasmidName;

  public GbkHeader() {
  }

  @Override
  public String toString() {
    return "GbkHeader{" +
        "bioProjectId='" + bioProjectId + '\'' +
        ", assemblyId='" + assemblyId + '\'' +
        ", assemblyName='" + assemblyName + '\'' +
        ", bioSampleId='" + bioSampleId + '\'' +
        ", locus='" + locus + '\'' +
        ", definition='" + definition + '\'' +
        ", accessions='" + accessions + '\'' +
        ", version='" + version + '\'' +
        ", organismName='" + organismName + '\'' +
        ", organismFullText='" + organismFullText + '\'' +
        ", length=" + length +
        '}';
  }

  public String getLocus() {
    return locus;
  }

  public void setLocus(String locus) {
    this.locus = locus;
  }

  public String getDefinition() {
    return definition;
  }

  public void setDefinition(String definition) {
    this.definition = definition;
  }

  public List<String> getAccessions() {
    return accessions;
  }

  public void setAccessions(List<String> accessions) {
    this.accessions = accessions;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getOrganismName() {
    return organismName;
  }

  public void setOrganismName(String organismName) {
    this.organismName = organismName;
  }

  public String getOrganismFullText() {
    return organismFullText;
  }

  public void setOrganismFullText(String organismFullText) {
    this.organismFullText = organismFullText;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public int getLength() {
    return length;
  }

  public void setLength(int length) {
    this.length = length;
  }

  public String getTaxonId() {
    return taxonId;
  }

  public void setTaxonId(String taxonId) {
    this.taxonId = taxonId;
  }

  public String getSubSpeciesName() {
    return subSpeciesName;
  }

  public void setSubSpeciesName(String subSpeciesName) {
    this.subSpeciesName = subSpeciesName;
  }

  public String getStrainName() {
    return strainName;
  }

  public void setStrainName(String strainName) {
    this.strainName = strainName;
  }

  public String getPlasmidName() {
    return plasmidName;
  }

  public void setPlasmidName(String plasmidName) {
    this.plasmidName = plasmidName;
  }

  public String getBioProjectId() {
    return bioProjectId;
  }

  public void setBioProjectId(String bioProjectId) {
    this.bioProjectId = bioProjectId;
  }

  public String getAssemblyId() {
    return assemblyId;
  }

  public void setAssemblyId(String assemblyId) {
    this.assemblyId = assemblyId;
  }

  public String getBioSampleId() {
    return bioSampleId;
  }

  public void setBioSampleId(String bioSampleId) {
    this.bioSampleId = bioSampleId;
  }

  public String getAssemblyName() {
    return assemblyName;
  }

  public void setAssemblyName(String assemblyName) {
    this.assemblyName = assemblyName;
  }
}
