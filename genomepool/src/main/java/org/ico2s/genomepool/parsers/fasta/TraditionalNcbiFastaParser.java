/*
 * Copyright 2013 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.parsers.fasta;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.logging.Logger;

/**
 * A FASTA parser for use with files obtained from the NCBI, where the ID line might contain multiple identifiers.
 * In this case, each identifier is usually separated by a '|' character.  For example:
 * <pre>
 *   >gi|387144427|ref|YP_005759109.1| cadmium efflux regulator [Staphylococcus aureus subsp. aureus TW20]
 * </pre>
 *
 * @author Keith Flanagan
 */
public class TraditionalNcbiFastaParser {
  private static final Logger logger = Logger.getLogger(TraditionalNcbiFastaParser.class.getName());


  public List<FastaEntry> parse(InputStream is) throws IOException {
    return parse(new BufferedReader(new InputStreamReader(is)));
  }

  public List<FastaEntry> parse(BufferedReader br) throws IOException {
    List<FastaEntry> entries = new ArrayList<>(1000);

    FastaEntry current = null;
    StringBuilder sequence = null;
    for (String line = br.readLine(); line != null; line = br.readLine()) {
      if (line.startsWith(">")) {
        if (current != null) {
          current.setSequence(sequence.toString());
          entries.add(current);
        }
        current = new FastaEntry();
        current.setFullIdLine(line);
        StringTokenizer st = new StringTokenizer(line, ">\t ");
        current.setFullId(st.nextToken());
        current.setDescription(st.nextToken());

        //current.setRawParsedIds(parseRawFastaSubIds(current.getFullId()));
        current.setRawParsedIds(parseRawFastaSubIds(line));
        current.setConvertedParsedIds(convertSubIds(current.getRawParsedIds()));

        sequence = new StringBuilder(1024);
      } else {
        if (sequence != null) {
          sequence.append(line);
        } else {
          logger.warning("Ignoring start of file that does not contain a FASTA ID line:\n"+line);
        }
      }
    }

    if (current != null) {
      current.setSequence(sequence.toString());
      entries.add(current);
    }

    return entries;
  }


  /**
   * Parse a FASTA ID, or 'id line' into |-separated sub-ids.
   *
   * For example, this:
   * <pre>
   *   >gi|387144427|ref|YP_005759109.1| cadmium efflux regulator [Staphylococcus aureus subsp. aureus TW20]
   * </pre>
   *
   * is turned into a Map of:
   * <pre>
   *   gi   ---> 387144427
   *   ref  ---> YP_005759109.1
   * </pre>
   *
   * @param fastaId the raw FASTA ID, or the entire ID line from a FASTA file.
   * @return a Map of IDs
   */
  public static Map<String, String> parseRawFastaSubIds(String fastaId) {

    // This part gets rid of the leading '>' and the final comments part of a header line
    StringTokenizer st = new StringTokenizer(fastaId, "> ");
    String allIds = st.nextToken();

    /*
     * We're left with one or more IDs for the sequence.
     * It seems to be the convention in Genbank/Embl files to separate ID types and IDs with '|'
     */
    Map<String, String> parsedIds = new HashMap<>();
    final String idSeparator = "|";
    st = new StringTokenizer(allIds, idSeparator);
    while (st.hasMoreTokens()) {
      String idType = st.nextToken();
      String idContent = st.nextToken();

      switch (idType) {
        case "gi" :
          parsedIds.put("gi", idContent);
          break;
        case "ref" :
          parsedIds.put("ref", idContent);
          break;
        default:
          logger.info("Warning: skipping unsupported FASTA ID type: "+idType+" with content: "+idContent);
      }

    }

    return parsedIds;
  }

  /**
   * Takes a set of parsed FASTA entry sub IDs (for example, as parsed by <code>parseRawFastaSubIds</code> and
   * converts the key names into those compatible with the parsed db_xref entries found in Genbank files.
   *
   *
   * For example, this:
   * <pre>
   *   gi        ---> 387144427
   *   ref       ---> YP_005759109.1
   * </pre>
   *
   * ... is turned into a Map of:
   * <pre>
   *   GI        ---> 387144427          (refers to a dbxref entry of GI: in a Genbank file)
   *   ProteinID ---> YP_005759109.1     (refers to a protein_id value of a CDS in a Genbank file)
   * </pre>
   *
   * The Map keys relate to the key IDs that the <code>GbkToEntanglement</code> parser uses when parsing Genbank
   * files into graph structures.
   *
   * @param parsedRawSubIds a map of the IDs parsed from a FASTA ID line.
   * @return a Map of IDs
   */
  public static Map<String, String> convertSubIds(Map<String, String> parsedRawSubIds) {
    Map<String, String> converted = new HashMap<>();
    for (Map.Entry<String, String> entry : parsedRawSubIds.entrySet()) {
      switch (entry.getKey()) {
        case "gi":
          converted.put("GI", entry.getValue());
          break;
        case "ref":
          converted.put("ProteinID", entry.getValue());
          break;
        default:
          logger.info("Warning: skipping unsupported FASTA ID type: " + entry.getKey() + " with content: " + entry.getValue());
      }
    }

    return converted;
  }

}
