/*
 * Copyright 2013 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.parsers.genbank.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Keith Flanagan
 */
public class Feature implements Serializable {
  protected String type;
  protected Location location;
  protected String locusTag;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  protected String geneName;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  protected String note;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  protected Set<String> dbXrefs;

  public Feature() {
    this.dbXrefs = new HashSet<>();
  }

  @Override
  public String toString() {
    return "Feature{" +
        "type='" + type + '\'' +
        ", locusTag='" + locusTag + '\'' +
        ", location=" + location +
        ", geneName=" + geneName +
        ", dbXrefs=" + dbXrefs +
        ", note='" + note + '\'' +
        '}';
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public String getLocusTag() {
    return locusTag;
  }

  public void setLocusTag(String locusTag) {
    this.locusTag = locusTag;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public Set<String> getDbXrefs() {
    return dbXrefs;
  }

  public void setDbXrefs(Set<String> dbXrefs) {
    this.dbXrefs = dbXrefs;
  }

  public String getGeneName() {
    return geneName;
  }

  public void setGeneName(String geneName) {
    this.geneName = geneName;
  }

  /**
   * A CDS entry parsed from an NCBI Genbank format file, or, alternatively, an NCBI-format FAA file.
   * <p/>
   * An example entry might look like this:
   * <pre>
   *  gene  complement(1134..1586)
   *        /locus_tag="BCK_26243"
   *        /db_xref="GeneID:13490435"
   *  CDS   complement(1134..1586)
   *        /locus_tag="BCK_26243"
   *        /codon_start=1
   *        /transl_table=11
   *        /product="bcla protein"
   *        /protein_id="YP_006599264.1"
   *        /db_xref="GI:402557994"
   *        /db_xref="GeneID:13490435"
   *        /translation="MQNVLQQLIGETVLLGTIADAPNVPPLFFLFTITSVNDFLVTVT
   *        GGATSYVVNISDVTGVGFLPPGPSITLLPPVDLGCECDCRERPIRELLDTLIGSTVNL
   *        LASTGSTAADFNVEQTGLGIVLGTLPISPTTIVRFAISTCKITAVNIL"
   * </pre>
   *
   * Alternatively, CDS entries may be created by parsing FASTA format files from NCBI that contain protein sequences.
   * Here, the ID line of the FASTA files have a very specific format within the ASCII-art. For example:
   * <pre>
   *    >gi|402557994|ref|YP_006599264.1| bcla protein [Bacillus cereus FRI-35]
   *    MQNVLQQLIGETVLLGTIADAPNVPPLFFLFTITSVNDFLVTVTGGATSYVVNISDVTGVGFLPPGPSIT
   *    LLPPVDLGCECDCRERPIRELLDTLIGSTVNLLASTGSTAADFNVEQTGLGIVLGTLPISPTTIVRFAIS
   *    TCKITAVNIL
   * </pre>
   *
   * There are a few things to note here. Although the IDs from the two entries above match, they are formatted
   * slightly differently - you can't just copy the strings and expect things to integrate. You need to reformat
   * the string in Entanglement.
   *
   * @author Keith Flanagan
   */
  public static class CDS extends Feature {
    private String proteinId;
    private String product;
    private String translation;


    @Override
    public String toString() {
      return "Feature{" +
          "type='" + type + '\'' +
          ", locusTag='" + locusTag + '\'' +
          ", location=" + location +
          " proteinId='" + proteinId + '\'' +
          " product='" + product + '\'' +
          ", dbXrefs=" + dbXrefs +
          ", note='" + note + '\'' +
          ", translation='" + translation + '\'' +
          '}';
    }

    public CDS() {
    }

    public String getProduct() {
      return product;
    }

    public void setProduct(String product) {
      this.product = product;
    }

    public String getTranslation() {
      return translation;
    }

    public void setTranslation(String translation) {
      this.translation = translation;
    }

    public String getProteinId() {
      return proteinId;
    }

    public void setProteinId(String proteinId) {
      this.proteinId = proteinId;
    }
  }


  public static class Gene extends Feature {

  }

  public static class MiscFeature extends Feature {

  }

}
