/*
 * Copyright 2013 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.parsers.genbank.data;

import java.io.Serializable;

/**
 * Represents an organism referred to by a GenBank (or other) data file.
 *
 * @author Keith Flanagan
 */
public class Organism implements Serializable {
  private String organismDirName; // Apparently the only reliable identifier for organisms across Genbank(!)
  private String organismName;
  private String fullOrganismText;

  private String taxonId;
  private String subSpeciesName;
  private String strainName;

  public Organism() {
  }

  @Override
  public String toString() {
    return "Organism{" +
        "organismDirName='" + organismDirName + '\'' +
        ", taxonId='" + taxonId + '\'' +
        ", organismName='" + organismName + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Organism organism = (Organism) o;

    if (organismDirName != null ? !organismDirName.equals(organism.organismDirName) : organism.organismDirName != null)
      return false;

    return true;
  }

  @Override
  public int hashCode() {
    return organismDirName != null ? organismDirName.hashCode() : 0;
  }

  public String getOrganismName() {
    return organismName;
  }

  public void setOrganismName(String organismName) {
    this.organismName = organismName;
  }

  public String getFullOrganismText() {
    return fullOrganismText;
  }

  public void setFullOrganismText(String fullOrganismText) {
    this.fullOrganismText = fullOrganismText;
  }

  public String getTaxonId() {
    return taxonId;
  }

  public void setTaxonId(String taxonId) {
    this.taxonId = taxonId;
  }

  public String getSubSpeciesName() {
    return subSpeciesName;
  }

  public void setSubSpeciesName(String subSpeciesName) {
    this.subSpeciesName = subSpeciesName;
  }

  public String getStrainName() {
    return strainName;
  }

  public void setStrainName(String strainName) {
    this.strainName = strainName;
  }

  public String getOrganismDirName() {
    return organismDirName;
  }

  public void setOrganismDirName(String organismDirName) {
    this.organismDirName = organismDirName;
  }
}
