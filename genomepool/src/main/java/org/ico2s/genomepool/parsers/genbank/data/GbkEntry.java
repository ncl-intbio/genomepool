/*
 * Copyright 2013 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.parsers.genbank.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Keith Flanagan
 */
public class GbkEntry implements Serializable {
  private GbkHeader header;
  private List<Feature> features;
  private String dna;

  public GbkEntry() {
    this.header = new GbkHeader();
    this.features = new ArrayList<>();
  }

  @Override
  public String toString() {
    return "GbkEntry{" +
        "header=" + header.getAccessions() + " | " + header.getVersion() + " | " + header.getOrganismName() + " | " +
        header.getSubSpeciesName() + " | " + header.getStrainName() +
        ", features=" + features.size() +
        ", dna='" + dna.length() + '\'' +
        '}';
  }

  public GbkHeader getHeader() {
    return header;
  }

  public void setHeader(GbkHeader header) {
    this.header = header;
  }

  public List<Feature> getFeatures() {
    return features;
  }

  public void setFeatures(List<Feature> features) {
    this.features = features;
  }

  public String getDna() {
    return dna;
  }

  public void setDna(String dna) {
    this.dna = dna;
  }
}
