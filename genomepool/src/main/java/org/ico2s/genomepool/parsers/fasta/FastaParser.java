/*
 * Copyright 2013 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.parsers.fasta;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.logging.Logger;

/**
 * A FASTA parser for use with files obtained from the NCBI, where the ID line contains a <i>single</i>
 * identifier. This is the new format as of 2015/16.
 *
 * For example:
 * <pre>
 *   >WP_003151955.1 MULTISPECIES: nitrogen-fixing protein NifU [Bacillales]
 * </pre>
 *
 * @author Keith Flanagan
 */
public class FastaParser {
  private static final Logger logger = Logger.getLogger(FastaParser.class.getName());


  public List<FastaEntry> parse(InputStream is) throws IOException {
    try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
      return parse(br);
    }
  }

  public List<FastaEntry> parse(BufferedReader br) throws IOException {
    List<FastaEntry> entries = new ArrayList<>(1000);

    FastaEntry current = null;
    StringBuilder sequence = null;
    for (String line = br.readLine(); line != null; line = br.readLine()) {
      if (line.startsWith(">")) {
        if (current != null) {
          current.setSequence(sequence.toString());
          current.setSequenceLength(current.getSequence().length());
          entries.add(current);
        }
        current = new FastaEntry();
        current.setFullIdLine(line);
        StringTokenizer st = new StringTokenizer(line, ">\t ");
        String idToken = st.nextToken();
        current.setFullId(idToken);
        int descriptionStart = idToken.length()+2; //+2: one for '>', one for trailing description
        current.setDescription(descriptionStart >= line.length() ? "" : line.substring(descriptionStart));

        current.setRawParsedIds(new HashMap<>()); // Nothing to do here for the new NCBI FASTA format files.

        // Construct a 'typed' ID of the same form as used by IDs parsed by other classes into Entanglement.
        // With the new FASTA file layout, there is now only one such ID.
        Map<String, String> singletonId = new HashMap<>();
        singletonId.put("ProteinID", idToken);
        current.setConvertedParsedIds(singletonId);

        sequence = new StringBuilder(1024);
      } else {
        if (sequence != null) {
          sequence.append(line);
        } else {
          logger.warning("Ignoring start of file that does not contain a FASTA ID line:\n"+line);
          throw new IOException("Failed to find recognisable FASTA header: "+line);
        }
      }
    }

    if (current != null) {
      current.setSequence(sequence.toString());
      current.setSequenceLength(current.getSequence().length());
      entries.add(current);
    }

    return entries;
  }
}
