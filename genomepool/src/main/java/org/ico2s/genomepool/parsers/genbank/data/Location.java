/*
 * Copyright 2013 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.parsers.genbank.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Keith Flanagan
 */
public class Location implements Serializable {

  public static class Region implements Serializable {
    private int start;
    private int end;
    private boolean complement;

    public Region() {
    }

    public Region(int start, int end, boolean complement) {
      this.start = start;
      this.end = end;
      this.complement = complement;
    }

    @Override
    public String toString() {
      return "Region{" +
          "start=" + start +
          ", end=" + end +
          ", complement=" + complement +
          '}';
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      Region region = (Region) o;

      if (complement != region.complement) return false;
      if (end != region.end) return false;
      if (start != region.start) return false;

      return true;
    }

    @Override
    public int hashCode() {
      int result = start;
      result = 31 * result + end;
      result = 31 * result + (complement ? 1 : 0);
      return result;
    }

    public int getStart() {
      return start;
    }

    public void setStart(int start) {
      this.start = start;
    }

    public int getEnd() {
      return end;
    }

    public void setEnd(int end) {
      this.end = end;
    }

    public boolean isComplement() {
      return complement;
    }

    public void setComplement(boolean complement) {
      this.complement = complement;
    }
  }

  private List<Region> regions;

  public Location() {
    this.regions = new ArrayList<>();
  }

  @Override
  public String toString() {
    return "Location{" +
        "regions=" + regions +
        '}';
  }

  public void addRegion(Region region) {
    regions.add(region);
  }

  public Region lowestStart() {
    if (regions.isEmpty()) {
      throw new RuntimeException("No Regions exist");
    }
    List<Region> sortedByStart = new ArrayList<>(regions);
    Collections.sort(sortedByStart, (o1, o2) -> Integer.compare(o1.getStart(), o2.getStart()));
    return sortedByStart.get(0);
  }

  public Region highestEnd() {
    if (regions.isEmpty()) {
      throw new RuntimeException("No Regions exist");
    }
    List<Region> sortedByStart = new ArrayList<>(regions);
    Collections.sort(sortedByStart, (o1, o2) -> Integer.compare(o1.getEnd(), o2.getEnd()));
    return sortedByStart.get(sortedByStart.size()-1);
  }

  public List<Region> getRegions() {
    return regions;
  }

  public void setRegions(List<Region> regions) {
    this.regions = regions;
  }
}
