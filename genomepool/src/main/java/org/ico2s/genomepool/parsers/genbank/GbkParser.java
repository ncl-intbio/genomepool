/*
 * Copyright 2013 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.parsers.genbank;

import org.ico2s.genomepool.parsers.genbank.data.Feature;
import org.ico2s.genomepool.parsers.genbank.data.GbkEntry;
import org.ico2s.genomepool.parsers.genbank.data.GbkHeader;
import org.ico2s.genomepool.parsers.genbank.data.Location;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;

/**
 * Parses a Genbank-formatted stream of data.
 *
 * This parser is not thread safe. Use multiple instances to parse different files in parallel.
 *
 * @author Keith Flanagan
 */
public class GbkParser {

  // 5 spaces used to introduce a new FEATURE instance
  private String DEFAULT_GBK_FEATURE_INDENT = "     ";
  // 21 spaces used between the start of a line and an attribute of a FEATURE
  private String DEFAULT_GBK_FEATURE_PROPERTY_INDENT = "                     ";

  /**
   * State implementations are responsible for either parsing a portion of a file, or delegating onto another
   * implementation for parsing.
   */
  private interface State {
    /**
     * The 'current' state (entry at the top of the stack) is handed new lines as they arrive from the stream.
     * Your state implementation may either choose to process <code>line</code> itself, or alternatively, delegate
     * onto another <code>State</code> object to process it (either by creating a new nested child state, or by
     * passing the line back to the parent state).
     *
     * As a convention, <code>nextLine</code> implementations should handle state exit conditions first, and then
     * handle parsing or delegation conditions. See the examples below for details.
     *
     * @param line a line of a Genbank entry
     * @throws ParserException
     */
    public void nextLine(String line) throws ParserException;
  }


  /**
   * Represents the various top-level states for parsing an entire Genbank entry (note that there may be multiple
   * entries per file or stream).
   *
   * A Genbank entry consists of 3 main blocks (draft sequences with scaffold entries are not yet supported):
   * 1) A header block, containing organism, fragment and author information.
   * 2) A list of features (gene, CDS, etc)
   * 3) A DNA sequence (marked by the keyword 'ORIGIN')
   *
   * Finally, a Genbank entry is terminated with the marker: '//'
   */
  private class GenbankEntryParser implements State {
    private final GbkEntry gbkEntry;
    private boolean firstLine = true;

    private GenbankEntryParser(GbkEntry gbkEntry) {
      this.gbkEntry = gbkEntry;
    }

    @Override
    public void nextLine(String line) throws ParserException {
      if (line.startsWith("//") && !firstLine) {
        // Exit - we've reached the end of the genbank entry. Pop the state and pass the unparsed line onwards
        // TODO fire event?
        stateStack.pop();
        if (!stateStack.isEmpty()) {
          // If this is a multi-entry parser, then continue parsing
          stateStack.peek().nextLine(line); // Pass unparsed line back to parent state
        }
      }
      // Dispatch sub-states / parsers
      else if (line.startsWith("//")) {
        // Do nothing - skip over this line (if it happens to be sent to us)
      }
      else if (line.startsWith("LOCUS")) {
        // Start of the GenBank "header" block (metadata before the FEATURES)
        HeaderBlockParser state = new HeaderBlockParser(gbkEntry);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (line.startsWith("FEATURES")) {
        // Start of the GenBank "features" block
        FeatureBlockParser state = new FeatureBlockParser(gbkEntry);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (line.startsWith("ORIGIN")) {
        // Start of the GenBank "ORIGIN" block (DNA sequence)
        OriginBlockParser state = new OriginBlockParser(gbkEntry);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      }
      firstLine = false;
    }
  }

  /**
   * Dispatches parsers for reading Genbank file metadata (the block of lines containing organism info, publication
   * data, author comments, etc).
   *
   * The termination of the header block is assumed to be a line of text starting with the string 'FEATURES'.
   */
  private class HeaderBlockParser implements State {
    private final GbkEntry gbkEntry;

    private HeaderBlockParser(GbkEntry gbkEntry) {
      this.gbkEntry = gbkEntry;
    }

    @Override
    public void nextLine(String line) throws ParserException {
      if (line.startsWith("FEATURES")) {
        // Exit - we've reached the end of the header block. Pop the state and pass the unparsed line onwards
        stateStack.pop();
        stateStack.peek().nextLine(line); // Pass unparsed line back to parent state
      }
      // Dispatch sub-states / parsers
      else if (line.startsWith("LOCUS")) {
        LocusParser state = new LocusParser(gbkEntry.getHeader());
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      }
      else if (line.startsWith("DEFINITION")) {
        DefinitionParser state = new DefinitionParser(gbkEntry.getHeader());
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      }
      else if (line.startsWith("DBLINK")) {
        DbLinkParser state = new DbLinkParser(gbkEntry.getHeader());
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      }
      else if (line.startsWith("ACCESSION")) {
        AccessionParser state = new AccessionParser(gbkEntry.getHeader());
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      }
      else if (line.startsWith("VERSION")) {
        VersionParser state = new VersionParser(gbkEntry.getHeader());
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      }
      else if (line.trim().startsWith("ORGANISM")) {
        // Trim used here because we're cheating slightly - ORGANISM is actually a sub-block of SOURCE
        OrganismParser state = new OrganismParser(gbkEntry.getHeader());
        stateStack.push(state);
        stateStack.peek().nextLine(line.trim()); // Pass unparsed line to child state
      }
      else if (line.startsWith("COMMENT")) {
        CommentParser state = new CommentParser(gbkEntry.getHeader());
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      }
    }
  }

  /**
   * An abstract class for dealing with top-level single/multi-line text entries. This includes most entries from
   * the Genbank header. Examples: LOCUS/DEFINITION/VERSION/COMMENT fields. A characteristic of these fields are
   * multi-line text that is NOT enclosed within double-quotes (unlike some other fields later on in the file).
   */
  abstract private class AbstractMultilineTextParser implements State {
    private final String keyName;
    protected final StringBuilder text;

    private AbstractMultilineTextParser(String keyName) {
      this.keyName = keyName;
      this.text = new StringBuilder();
    }

    @Override
    public void nextLine(String line) throws ParserException {
      if (!line.startsWith(keyName) && !startsWithWhiteSpace(line)) {
        // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
        done(text.toString()); // Allow the implementation to fire events, set data fields, etc.
        stateStack.pop();
        stateStack.peek().nextLine(line); // Pass unparsed line back to parent state
      }
      // Parse
      else if (line.startsWith(keyName)) {
        text.append(line.substring(keyName.length()).trim());
      } else {
        text.append(" ").append(line.trim());
      }
    }

    abstract protected void done(String parsedText);
  }

  private class LocusParser extends AbstractMultilineTextParser {
    private final GbkHeader gbkHeader;

    private LocusParser(GbkHeader gbkHeader) {
      super("LOCUS");
      this.gbkHeader = gbkHeader;
    }

    @Override
    protected void done(String parsedText) {
      // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
      // TODO fire event?
      gbkHeader.setLocus(parsedText);
    }
  }

  private class DefinitionParser extends AbstractMultilineTextParser {
    private final GbkHeader gbkHeader;

    private DefinitionParser(GbkHeader gbkHeader) {
      super("DEFINITION");
      this.gbkHeader = gbkHeader;
    }

    @Override
    protected void done(String parsedText) {
      // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
      // TODO fire event?
      gbkHeader.setDefinition(parsedText);
    }
  }

  private class DbLinkParser implements State {

    private static final String KEY_NAME = "DBLINK";
    private final GbkHeader gbkHeader;

    private DbLinkParser(GbkHeader gbkHeader) {
      this.gbkHeader = gbkHeader;
    }

    @Override
    public void nextLine(String line) throws ParserException {
      if (!line.startsWith(KEY_NAME) && !startsWithWhiteSpace(line)) {
        // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
        stateStack.pop();
        stateStack.peek().nextLine(line); // Pass unparsed line back to parent state
        return;
      }

      // Parse
      if (line.startsWith(KEY_NAME)) {
        line = line.substring(KEY_NAME.length()).trim();
      }
      if (line.startsWith("BioProject:")) {
        gbkHeader.setBioProjectId(line.split(" ")[1]);
      } else if (line.startsWith("Assembly:")) {
        gbkHeader.setAssemblyId(line.split(" ")[1]);
      } else if (line.startsWith("BioSample:")) {
        gbkHeader.setBioSampleId(line.split(" ")[1]);
      }
    }
  }

  /**
   * In some cases, there are multiple accession numbers per fragment. This looks like:
   *
   * <pre>
   *   LOCUS       NC_002978            1267782 bp    DNA     circular CON 12-AUG-2015
   *   DEFINITION  Wolbachia endosymbiont of Drosophila melanogaster, complete genome.
   *   ACCESSION   NC_002978 NZ_AE017256 NZ_AE017257 NZ_AE017258 NZ_AE017259
   *               NZ_AE017260
   *   VERSION     NC_002978.6  GI:42519920
   * </pre>
   */
  private class AccessionParser extends AbstractMultilineTextParser {
    private final GbkHeader gbkHeader;

    private AccessionParser(GbkHeader gbkHeader) {
      super("ACCESSION");
      this.gbkHeader = gbkHeader;
    }

    @Override
    protected void done(String parsedText) {
      // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
      List<String> accessions = Arrays.asList(parsedText.split(" "));
      gbkHeader.setAccessions(accessions);
    }
  }

  /**
   * Currently, there might be multiple versions per Genbank-formatted entry. Ultimately, NCBI are removing
   * the "GI" numbers. We need to take these into account at the moment, but ignore them.
   *
   * For example:
   * <pre>
   *   VERSION     NC_002978.6  GI:42519920
   * </pre>
   */
  private class VersionParser extends AbstractMultilineTextParser {
    private final GbkHeader gbkHeader;

    private VersionParser(GbkHeader gbkHeader) {
      super("VERSION");
      this.gbkHeader = gbkHeader;
    }

    @Override
    protected void done(String parsedText) {
      String[] versionTokens = parsedText.split(" ");

      // Take the first version token - this is always of the form "ACCESSION.VERSION"
      gbkHeader.setVersion(versionTokens[0]);
    }
  }

  private class OrganismParser extends AbstractMultilineTextParser {
    private final GbkHeader gbkHeader;

    private OrganismParser(GbkHeader gbkHeader) {
      super("ORGANISM");
      this.gbkHeader = gbkHeader;
    }

    @Override
    protected void done(String parsedText) {
      /*
       * parsedText contains the entire multi-line ORGANISM sub-block.
       * That might look like this:
       *     ORGANISM    Bacillus cereus FRI-35
       *         Bacteria; Firmicutes; Bacilli; Bacillales; Bacillaceae; Bacillus;
       *         Bacillus cereus group.
       * Here, we set the full, unmodified text on the GBK header object.
       * We also parse out the first line of this and set that as the organism name.
       */
      gbkHeader.setOrganismFullText(parsedText);

      //StringTokenizer st = new StringTokenizer(parsedText, "\\n");
      //gbkHeader.setOrganismName(st.nextToken());
    }
  }

  private class CommentParser extends AbstractMultilineTextParser {
    private final GbkHeader gbkHeader;

    private CommentParser(GbkHeader gbkHeader) {
      super("COMMENT");
      this.gbkHeader = gbkHeader;
    }

    @Override
    protected void done(String parsedText) {
      // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
      // TODO fire event?
      gbkHeader.setComment(parsedText);
    }
  }

  /**
   * Dispatches parsers for feature types (CDS, gene, misc_feature, ...)
   *
   * The termination of the 'features' block is assumed when a line is encountered that contains 'unindented' text
   * (i.e., a line whose first character is NOT whitespace). We can't be more specific than this, since there are
   * multiple possibilities for what follows a 'features' block:
   *
   *   - Typically, but not always, the unindented string 'ORIGIN'.
   *   - Genbank entries for draft genomes may be different (eg, 'WGS' or 'WGS_SCAFLD').
   *   - Possibly the end-of-entry marker, '//'
   */
  private class FeatureBlockParser implements State {
    private final GbkEntry gbkEntry;

    private FeatureBlockParser(GbkEntry gbkEntry) {
      this.gbkEntry = gbkEntry;
    }

    @Override
    public void nextLine(String line) throws ParserException {
      String trimmed = line.trim();
      if (!startsWithWhiteSpace(line) && !line.startsWith("FEATURES")) {
        // Exit - we've overrun the 'FEATURES' block. Pop the state and pass the unparsed line onwards
        // TODO fire event?
        stateStack.pop();
        stateStack.peek().nextLine(line); // Pass unparsed line back to parent state
      }
      // Dispatch sub-states / parsers
      //else if (trimmed.startsWith("source")) {
      else if (line.startsWith(DEFAULT_GBK_FEATURE_INDENT+"source")) {
        // The first entry within the 'FEATURES' block is actually information about the fragment and organism
        SourceParser state = new SourceParser(gbkEntry);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      }
      //else if (trimmed.startsWith("CDS")) {
      else if (line.startsWith(DEFAULT_GBK_FEATURE_INDENT+"CDS")) {
        CdsParser state = new CdsParser(gbkEntry);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      }
      //else if (trimmed.startsWith("gene")) {
      else if (line.startsWith(DEFAULT_GBK_FEATURE_INDENT+"gene")) {
        GeneParser state = new GeneParser(gbkEntry);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      }
      //else if (trimmed.startsWith("misc_feature")) {
      else if (line.startsWith(DEFAULT_GBK_FEATURE_INDENT+"misc_feature")) {
        MiscFeatureParser state = new MiscFeatureParser(gbkEntry);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      }
    }
  }

  /**
   * Parses a CDS feature by composing elements parsed by a number of sub-parsers.
   */
  private class SourceParser implements State {
    private boolean sourceTagSpotted = false;
//    private Integer sourceStart;
//    private Integer sourceEnd;

    private final GbkEntry gbkEntry;

    private SourceParser(GbkEntry gbkEntry) {
      this.gbkEntry = gbkEntry;
    }

    @Override
    public void nextLine(String line) throws ParserException {
      String trimmed = line.trim();

      //if (sourceStart != null && !trimmed.startsWith("/")) {
      if (sourceTagSpotted && !line.startsWith(DEFAULT_GBK_FEATURE_PROPERTY_INDENT)) {
        // Exit - we've overrun the 'source' entry. Pop the state and pass the unparsed line onwards
        if (gbkEntry.getHeader().getTaxonId() == null) {
          throw new ParserException("Completed parsing the FEATURES/source section, but no taxon ID was found!");
        }
        // TODO fire event?
        stateStack.pop();                 // Pop *this* state off the stack
        stateStack.peek().nextLine(line); // Pass unparsed line back to parent state
      }
      // Dispatch sub-states / parsers
      else if (!sourceTagSpotted && trimmed.startsWith("source")) {
        // Starting the 'source' feature
        sourceTagSpotted = true;
//        StringTokenizer st = new StringTokenizer(trimmed.substring("source".length()), ".");
//        sourceStart = Integer.parseInt(st.nextToken().trim());
//        sourceEnd = Integer.parseInt(st.nextToken().trim());
      } else if (trimmed.startsWith("/organism=")) {
        //System.out.println("   *****  Parsing organism= line: "+trimmed);
        OrganismNameParser state = new OrganismNameParser(gbkEntry.getHeader());
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/db_xref=\"taxon")) {
        //System.out.println("   *****  Parsing taxon= line: "+trimmed);
        TaxonIdParser state = new TaxonIdParser(gbkEntry.getHeader());
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/sub_species=")) {
        SubSpeciesNameParser state = new SubSpeciesNameParser(gbkEntry.getHeader());
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/strain=")) {
        StrainNameParser state = new StrainNameParser(gbkEntry.getHeader());
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/plasmid=")) {
        PlasmidNameParser state = new PlasmidNameParser(gbkEntry.getHeader());
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else {
        // FIXME Ignore unknown items?
        //throw new ParserException("Unexpected or unsupported CDS sub-entry: "+line);
      }
    }
  }


  private class GeneParser implements State {
    private Feature.Gene gene = null;

    private final GbkEntry gbkEntry;

    private GeneParser(GbkEntry gbkEntry) {
      this.gbkEntry = gbkEntry;
    }

    @Override
    public void nextLine(String line) throws ParserException {
      String trimmed = line.trim();
      if (gene != null && !trimmed.startsWith("/")) {
        // Exit - we've overrun this CDS entry. Pop the state and pass the unparsed line onwards
        // TODO fire event?
        gbkEntry.getFeatures().add(gene);
        stateStack.pop();                 // Pop *this* state off the stack
        stateStack.peek().nextLine(line); // Pass unparsed line back to parent state
      }
      // Dispatch sub-states / parsers
      else if (gene == null && trimmed.startsWith("gene")) {
        // Starting a new CDS feature
        gene = new Feature.Gene();
        gene.setType("gene");
        // First line. Parse location(s)
        LocationParser state = new LocationParser(gene);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/gene=")) {
        GeneNameParser state = new GeneNameParser(gene);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/locus_tag=")) {
        LocusTagParser state = new LocusTagParser(gene);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/db_xref=")) {
        DbXrefParser state = new DbXrefParser(gene);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/note=")) {
        NoteParser state = new NoteParser(gene);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else {
        // FIXME Ignore unknown items?
        //throw new ParserException("Unexpected or unsupported CDS sub-entry: "+line);
      }
    }
  }

  /**
   * Parses a CDS feature by composing elements parsed by a number of sub-parsers.
   */
  private class CdsParser implements State {
    private Feature.CDS cds = null;

    private final GbkEntry gbkEntry;

    private CdsParser(GbkEntry gbkEntry) {
      this.gbkEntry = gbkEntry;
    }

    @Override
    public void nextLine(String line) throws ParserException {
      String trimmed = line.trim();
      if (cds != null && !trimmed.startsWith("/")) {
        // Exit - we've overrun this CDS entry. Pop the state and pass the unparsed line onwards
        // TODO fire event?
        gbkEntry.getFeatures().add(cds);
        stateStack.pop();                 // Pop *this* state off the stack
        stateStack.peek().nextLine(line); // Pass unparsed line back to parent state
      }
      // Dispatch sub-states / parsers
      else if (cds == null && trimmed.startsWith("CDS")) {
        // Starting a new CDS feature
        cds = new Feature.CDS();
        cds.setType("CDS");
        // First line. Parse location(s)
        LocationParser state = new LocationParser(cds);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/gene=")) {
        GeneNameParser state = new GeneNameParser(cds);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/locus_tag=")) {
        LocusTagParser state = new LocusTagParser(cds);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/db_xref=")) {
        DbXrefParser state = new DbXrefParser(cds);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/note=")) {
        NoteParser state = new NoteParser(cds);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/product=")) {
        ProductParser state = new ProductParser(cds);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/protein_id=")) {
        ProteinIdParser state = new ProteinIdParser(cds);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/translation=")) {
        TranslationParser state = new TranslationParser(cds);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else {
        // FIXME Ignore unknown items?
        //throw new ParserException("Unexpected or unsupported CDS sub-entry: "+line);
      }
    }
  }

  private class MiscFeatureParser implements State {
    private Feature feature = null;

    private final GbkEntry gbkEntry;

    private MiscFeatureParser(GbkEntry gbkEntry) {
      this.gbkEntry = gbkEntry;
    }

    @Override
    public void nextLine(String line) throws ParserException {
      String trimmed = line.trim();
      if (feature != null && !trimmed.startsWith("/")) {
        // Exit - we've overrun this CDS entry. Pop the state and pass the unparsed line onwards
        // TODO fire event?
        gbkEntry.getFeatures().add(feature);
        stateStack.pop();                 // Pop *this* state off the stack
        stateStack.peek().nextLine(line); // Pass unparsed line back to parent state
      }
      // Dispatch sub-states / parsers
      else if (feature == null && trimmed.startsWith("misc_feature")) {
        // Starting a new CDS feature
        feature = new Feature();
        feature.setType("misc_feature");
        // First line. Parse location(s)
        LocationParser state = new LocationParser(feature);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/gene=")) {
        GeneNameParser state = new GeneNameParser(feature);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/locus_tag=")) {
        LocusTagParser state = new LocusTagParser(feature);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/db_xref=")) {
        DbXrefParser state = new DbXrefParser(feature);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else if (trimmed.startsWith("/note=")) {
        NoteParser state = new NoteParser(feature);
        stateStack.push(state);
        stateStack.peek().nextLine(line); // Pass unparsed line to child state
      } else {
        // FIXME Ignore unknown items?
        //throw new ParserException("Unexpected or unsupported CDS sub-entry: "+line);
      }
    }
  }

  /**
   * Parses location data. A number of (all?) feature types contain location information, which can span multiple lines.
   * Location data is formatted in plain text, and can be nested within keywords, such as 'complement', 'join', and
   * 'order'.
   * Currently, only 'complement' has any effect on this parser.
   */
  private class LocationParser implements State {
    private final Feature feature;
    private boolean firstLine = true;
    private StringBuilder locationText = new StringBuilder(); // Concatenated location text (possibly multi-line)

    private LocationParser(Feature feature) {
      this.feature = feature;
    }

    @Override
    public void nextLine(String line) throws ParserException {
      String trimmed = line.trim();
      // If there are 2 space-separated tokens, this is either the first line, or we've finished
      StringTokenizer regularTokenizer = new StringTokenizer(trimmed);
      int tokenCount = regularTokenizer.countTokens();

      if (trimmed.startsWith("/") || (tokenCount == 2 && !firstLine)) {
        // Exit - we overshot the location info. Need to parse the location text, store the Location, and end this state
        Location location = parseLocation(locationText.toString());
        // TODO fire event?
        feature.setLocation(location);
        stateStack.pop();                 // Pop *this* state off the stack
        stateStack.peek().nextLine(line); // Pass unparsed line back to parent state
      }
      // Dispatch sub-states / parsers
      else if (tokenCount == 2) {
        regularTokenizer.nextToken();
        String lineLocText = regularTokenizer.nextToken(); // 2nd token is of interest
        locationText.append(lineLocText); // Cut out the location text (RHS) from the entire line
      } else {
        // Normal continuation line. Append the text
        locationText.append(trimmed);
      }
      firstLine = false;
    }
  }


  /**
   * An abstract class for dealing with top-level single/multi-line text entries. Examples: /note= or /translation= fields.
   * Text is enclosed in double-quote marks
   */
  abstract private class AbstractQuotedMultilineTextParser implements State {
    private final String fullStartString;
    private final boolean appendSpaceAfterNewline;
    protected final StringBuilder text;
    protected boolean firstLine = true;
    protected boolean unclosedQuotes = false;

    private AbstractQuotedMultilineTextParser(String keyName, boolean appendSpaceAfterNewline) {
      this.appendSpaceAfterNewline = appendSpaceAfterNewline;
      this.fullStartString = "/"+keyName+"=\"";
      this.text = new StringBuilder();
    }

    @Override
    public void nextLine(String line) throws ParserException {
      String trimmed = line.trim();
      String keyRemoved = trimmed.startsWith(fullStartString) ? trimmed.substring(fullStartString.length()) : trimmed;
//      System.out.println(":"+keyRemoved);

      if (!firstLine && !unclosedQuotes) {
        // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
        done(text.toString()); // Allow the implementation to fire events, set data fields, etc.
        stateStack.pop();
        stateStack.peek().nextLine(line); // Pass unparsed line back to parent state
      }
      // Parse
      else if (!line.endsWith("\"")) {
        unclosedQuotes = true;
        text.append(keyRemoved);
        if (appendSpaceAfterNewline) {
          text.append(" ");
        }
      }
      else {
        unclosedQuotes = false;
        text.append(keyRemoved.substring(0, keyRemoved.lastIndexOf("\"")));
      }

      firstLine = false;
    }

    abstract protected void done(String parsedText);
  }

  private class OrganismNameParser extends AbstractQuotedMultilineTextParser {
    private final GbkHeader gbkHeader;

    private OrganismNameParser(GbkHeader gbkHeader) {
      super("organism", false);
      this.gbkHeader = gbkHeader;
    }

    @Override
    protected void done(String parsedText) {
      // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
      gbkHeader.setOrganismName(parsedText);
    }
  }

  private class SubSpeciesNameParser extends AbstractQuotedMultilineTextParser {
    private final GbkHeader gbkHeader;

    private SubSpeciesNameParser(GbkHeader gbkHeader) {
      super("sub_species", false);
      this.gbkHeader = gbkHeader;
    }

    @Override
    protected void done(String parsedText) {
      // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
      gbkHeader.setSubSpeciesName(parsedText);
    }
  }

  private class StrainNameParser extends AbstractQuotedMultilineTextParser {
    private final GbkHeader gbkHeader;

    private StrainNameParser(GbkHeader gbkHeader) {
      super("strain", false);
      this.gbkHeader = gbkHeader;
    }

    @Override
    protected void done(String parsedText) {
      // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
      gbkHeader.setStrainName(parsedText);
    }
  }

  private class PlasmidNameParser extends AbstractQuotedMultilineTextParser {
    private final GbkHeader gbkHeader;

    private PlasmidNameParser(GbkHeader gbkHeader) {
      super("plasmid", false);
      this.gbkHeader = gbkHeader;
    }

    @Override
    protected void done(String parsedText) {
      // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
      gbkHeader.setPlasmidName(parsedText);
    }
  }

  private class TaxonIdParser extends AbstractQuotedMultilineTextParser {
    private final GbkHeader gbkHeader;

    private TaxonIdParser(GbkHeader gbkHeader) {
      super("db_xref", false);
      this.gbkHeader = gbkHeader;
    }

    @Override
    protected void done(String parsedText) {
      // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
      gbkHeader.setTaxonId(parsedText.substring("taxon:".length()));
    }
  }




  private class LocusTagParser extends AbstractQuotedMultilineTextParser {
    private final Feature feature;

    private LocusTagParser(Feature feature) {
      super("locus_tag", false);
      this.feature = feature;
    }

    @Override
    protected void done(String parsedText) {
      // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
      // TODO fire event?
      feature.setLocusTag(parsedText);
    }
  }

  private class GeneNameParser extends AbstractQuotedMultilineTextParser {
    private final Feature feature;

    private GeneNameParser(Feature feature) {
      super("gene", false);
      this.feature = feature;
    }

    @Override
    protected void done(String parsedText) {
      // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
      // TODO fire event?
      feature.setGeneName(parsedText);
    }
  }

  private class DbXrefParser extends AbstractQuotedMultilineTextParser {
    private final Feature feature;

    private DbXrefParser(Feature feature) {
      super("db_xref", false);
      this.feature = feature;
    }

    @Override
    protected void done(String parsedText) {
      // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
      // TODO fire event?
      feature.getDbXrefs().add(parsedText);
    }
  }

  private class NoteParser extends AbstractQuotedMultilineTextParser {
    private final Feature feature;

    private NoteParser(Feature feature) {
      super("note", true);
      this.feature = feature;
    }

    @Override
    protected void done(String parsedText) {
      // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
      // TODO fire event?
      feature.setNote(parsedText);
    }
  }

  private class ProductParser extends AbstractQuotedMultilineTextParser {
    private final Feature.CDS cds;

    private ProductParser(Feature.CDS cds) {
      super("product", true);
      this.cds = cds;
    }

    @Override
    protected void done(String parsedText) {
      // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
      // TODO fire event?
      cds.setProduct(parsedText);
    }
  }

  private class ProteinIdParser extends AbstractQuotedMultilineTextParser {
    private final Feature.CDS cds;

    private ProteinIdParser(Feature.CDS cds) {
      super("protein_id", true);
      this.cds = cds;
    }

    @Override
    protected void done(String parsedText) {
      // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
      // TODO fire event?
      cds.setProteinId(parsedText);
    }
  }

  private class TranslationParser extends AbstractQuotedMultilineTextParser {
    private final Feature.CDS cds;

    private TranslationParser(Feature.CDS cds) {
      super("translation", false);
      this.cds = cds;
    }

    @Override
    protected void done(String parsedText) {
      // Exit - we've reached the end of the text block. Pop the state and pass the unparsed line onwards
      // TODO fire event?
      cds.setTranslation(parsedText);
    }
  }




  /**
   * Responsible for parsing the part of the Genbank file that contains DNA sequence data.
   *
   * The termination of the 'ORIGIN' block is assumed when a line is encountered that contains 'unindented' text
   * (i.e., a line whose first character is NOT whitespace). We can't be more specific than this, since there are
   * multiple possibilities for what follows a 'features' block:
   *
   *   - Typically, but not always, the end-of-entry marker, '//'.
   *   - Genbank entries for draft genomes may be different (eg, 'WGS' or 'WGS_SCAFLD')?
   */
  private class OriginBlockParser implements State {
    private final GbkEntry gbkEntry;
    private final StringBuilder text = new StringBuilder();

    private OriginBlockParser(GbkEntry gbkEntry) {
      this.gbkEntry = gbkEntry;
    }
    @Override
    public void nextLine(String line) throws ParserException {
      if (!startsWithWhiteSpace(line) && !line.startsWith("ORIGIN")) {
        // Exit - we've reached the end of the DNA sequence. Pop the state and pass the unparsed line onwards
        // TODO fire event?
        gbkEntry.setDna(text.toString());
        stateStack.pop();
        stateStack.peek().nextLine(line); // Pass unparsed line back to parent state
      }
      // Dispatch sub-states / parsers
      else if (line.startsWith("ORIGIN")) {
        // Do nothing - skip over the ORIGIN line
      } else {
        StringTokenizer st = new StringTokenizer(line);
        st.nextToken(); // Ignore integer DNA location
        // Compile DNA sequence
        while (st.hasMoreTokens()) {
          text.append(st.nextToken());
        }
      }
    }
  }


  private static boolean startsWithWhiteSpace(String text) {
    return text.charAt(0) == ' ' || text.charAt(0) == '\t';
  }

  /**
   * A utility for converting location ASCII-art into a Location object with multiple Regions.
   *
   * @param locationText Genbank ASCII-art, possibly obtained from multiple lines in the file.
   * @return a Location
   */
  private static Location parseLocation(String locationText) {
    Location location = new Location();
    StringTokenizer tokens = new StringTokenizer(locationText, "(.,)<>", true);
    boolean expectingRange = false;

    boolean complement = false;
    while (tokens.hasMoreTokens()) {
      String token = tokens.nextToken();
      if (token.equals("complement")) {
        complement = true;
      } else if (token.equals("order")) {
        // Nothing to do for now. We might want to distinguish order/join later?
      } else if (token.equals("join")) {
        // Nothing to do for now. We might want to distinguish order/join later?
      } else if (token.equals("(")) {
        // Nothing to do for now.
      } else if (token.equals(")")) {
        // Nothing to do for now.
      } else if (token.equals(".")) {
        expectingRange = true;
      } else if (token.equals(",")) {
        // Nothing to do for now.
      } else if (token.equals("<")) {
        // Nothing to do for now.
      } else if (token.equals(">")) {
        // Nothing to do for now.
      } else {
        // Presumably we're left with integer coordinates now.
        int coord;
        try {
          coord = Integer.parseInt(token);
        } catch(NumberFormatException e) {
          throw new RuntimeException("Failed to parse Integer from: "+token
              +". Full location text was: "+locationText, e);
        }
        if (!expectingRange) {
          Location.Region region = new Location.Region();
          region.setComplement(complement);
          region.setStart(coord);
          region.setEnd(coord);
          location.addRegion(region);
        } else {
          List<Location.Region> regions = location.getRegions();
          regions.get(regions.size()-1).setEnd(coord);
          expectingRange = false;
        }

      }
    }
    return location;
  }

  /*
   * Configuration and operational state for the parser goes here.
   */
  private final Stack<State> stateStack;

  public GbkParser() {
    this.stateStack = new Stack<>();
  }


  /**
   * Given an input stream, parses the first entry found in the stream.
   *
   * @param br the reader to use. This reader is NOT closed after use. You can therefore call this method
   *           multiple times with the same reader to read all the Genbank entries in a file.
   * @return the first entry found, or NULL if the end of stream was reached.
   * @throws ParserException
   */
  public GbkEntry parseEntry(BufferedReader br) throws ParserException {
    GbkEntry gbkEntry = null;
    stateStack.clear();
    try {
      for (String line = br.readLine(); line != null; line = br.readLine()) {
        if (gbkEntry == null) {
          gbkEntry = new GbkEntry();
          stateStack.push(new GenbankEntryParser(gbkEntry));
        }
        stateStack.peek().nextLine(line);
        if (stateStack.isEmpty()) {
          break;
        }
      }
    } catch (IOException e) {
      throw new ParserException("Failed to parse input stream", e);
    }

    //System.out.println("Parsing complete...");

    //System.out.println(gbkEntry);
    //System.out.println("Comment: "+gbkEntry.getHeader().getComment());
    //System.out.println("Features parsed: "+gbkEntry.getFeatures().size());
    //System.out.println("DNA length: "+gbkEntry.getDna().length());
//    for (Feature feature : gbkEntry.getFeatures()) {
//      System.out.println("  * "+feature.toString());
//    }
    return gbkEntry;
  }

  /**
   * Convenience method that runs the parser from a specified <code>InputStream</code>
   * @param is the <code>InputStream</code> to read from
   * @return
   * @throws ParserException
   */
  @Deprecated
  public GbkEntry parseEntry(InputStream is) throws ParserException {
    try(BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
      return parseEntry(br);
    } catch (Exception e) {
      throw new ParserException("Failed to parse input from specified input stream", e);
    }
  }

  /**
   * Convenience method that runs the parser from a specified file
   * @param file the input file to use
   * @return
   * @throws ParserException
   */
  @Deprecated
  public GbkEntry parseEntry(File file) throws ParserException {
    try(BufferedReader br = new BufferedReader(new FileReader(file))) {
      return parseEntry(br);
    } catch (Exception e) {
      throw new ParserException("Failed to parse input file: "+file.getAbsolutePath(), e);
    }
  }

  @Deprecated
  public GbkEntry parseEntry(String filePath) throws ParserException {
    return parseEntry(new File(filePath));
  }

}
