/*
 * Copyright 2013 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.parsers.fasta;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Keith Flanagan
 */
public class FastaEntry implements Serializable {
  private String fullIdLine;  // The full, untouched ">ID DESC" line

  private String fullId;      // The full FASTA ID (excludes '>' and the description)
  private String description; // The description part of a FASTA ID line. This is separated from the ID by a space/tab

  private Map<String, String> rawParsedIds;          // Parses '|'-separated IDs that are commonly found in FASTA files
  private Map<String, String> convertedParsedIds;    // Matches the CDS IDs as parsed by the GBK -> Entanglement parser

  private int sequenceLength;

  private String sequence;

  /**
   * Convenience method that creates a FASTA-formatted string for this FastaEntry
   * @return
   */
  public String toFasta() {
    StringBuilder sb = new StringBuilder(fullIdLine.length() + sequence.length() + 2);
    sb.append(fullIdLine).append("\n");
    sb.append(sequence).append("\n");
    return sb.toString();
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getFullIdLine() {
    return fullIdLine;
  }

  public void setFullIdLine(String fullIdLine) {
    this.fullIdLine = fullIdLine;
  }

  public String getFullId() {
    return fullId;
  }

  public void setFullId(String fullId) {
    this.fullId = fullId;
  }

  public String getSequence() {
    return sequence;
  }

  public void setSequence(String sequence) {
    this.sequence = sequence;
  }

  public Map<String, String> getRawParsedIds() {
    return rawParsedIds;
  }

  public void setRawParsedIds(Map<String, String> rawParsedIds) {
    this.rawParsedIds = rawParsedIds;
  }

  public Map<String, String> getConvertedParsedIds() {
    return convertedParsedIds;
  }

  public void setConvertedParsedIds(Map<String, String> convertedParsedIds) {
    this.convertedParsedIds = convertedParsedIds;
  }

  public int getSequenceLength() {
    return sequenceLength;
  }

  public void setSequenceLength(int sequenceLength) {
    this.sequenceLength = sequenceLength;
  }
}
