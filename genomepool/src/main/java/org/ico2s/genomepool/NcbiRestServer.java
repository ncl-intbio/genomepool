/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.ico2s.genomepool;

import com.google.gson.Gson;
import com.microbasecloud.util.GsonFactory;
import org.ico2s.genomepool.parsers.assembly_summary.AssemblySummary;
import org.ico2s.genomepool.parsers.fasta.FastaEntry;

import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import static spark.Spark.*;

/**
 * @author Keith Flanagan
 */
public class NcbiRestServer {

  private static final Logger logger = Logger.getLogger(NcbiRestServer.class.getName());

  public static void startRestServer(int port, Ncbi ncbi) {
    Gson gson = GsonFactory.make();

    port(port);

    exception(Exception.class, (e, request, response) -> {
      e.printStackTrace();
      response.status(500);
      response.body("Exception: "+e.getMessage()+"\n");
    });



    get("/assembly/:accession", (req, res) -> {
      String accession = req.params(":accession");
      logger.info(String.format("getAssembly: %s", accession));

      AssemblySummary result = ncbi.getAssembly(accession);
      res.status(200); // OK
      return gson.toJson(result);
    });


    get("/countAssemblies", (req, res) -> {
      logger.info("countAssemblies");
      long result = ncbi.countAssemblies();
      res.status(200); // OK
      return gson.toJson(result);
    });

    get("/assemblies", (req, res) -> {
      logger.info("listAssemblies");
      Set<AssemblySummary> result = ncbi.listAssemblies();
      res.status(200); // OK
      return gson.toJson(result);
    });

    post("/assembly-search", (req, res) -> {
      logger.info("assembly-search: "+req.body());
      String searchText = req.body();

      Set<AssemblySummary> result = ncbi.textSearchAssemblies(searchText);
      res.status(200); // OK
      return gson.toJson(result);
    });

    get("/dna/:accession", (req, res) -> {
      String accession = req.params(":accession");
      logger.info(String.format("getDnaSequences: %s", accession));

      List<FastaEntry> result = ncbi.getDnaSequences(accession);
      res.status(200); // OK
      return gson.toJson(result);
    });

    get("/protein/:accession", (req, res) -> {
      String accession = req.params(":accession");
      logger.info(String.format("getProteinSequences: %s", accession));

      List<FastaEntry> result = ncbi.getProteinSequences(accession);
      res.status(200); // OK
      return gson.toJson(result);
    });

  }
}
