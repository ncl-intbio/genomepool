/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.ico2s.genomepool;

import org.apache.commons.cli.*;

import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import static org.apache.commons.cli.OptionBuilder.withLongOpt;

/**
 * @author Keith Flanagan
 */
public class NcbiRestServerLauncher {
  private static final Logger logger = Logger.getLogger(NcbiRestServerLauncher.class.getName());

  private static final int DEFAULT_PORT = 5000;

  private static void printHelpExit(Options options) {
    HelpFormatter formatter = new HelpFormatter();
    String cmdSyntax = "NcbiRestServerLauncher.sh";
    String header = "";
    String footer = "";
    int width = 80;
    formatter.printHelp(width, cmdSyntax, header, options, footer);
    System.exit(0);
  }

  public static void main(String[] args) throws Exception {
    CommandLineParser parser = new DefaultParser();

    Options options = new Options();

    options.addOption(withLongOpt("url").hasArgs().withDescription(
        "One or more URLs at which sequence data is available. " +
            "ftp://location/to/files/to/add/to/the/database. For example: ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria, or " +
            "ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/Staphylococcus_aureus/").create());
    options.addOption(withLongOpt("local-storage").hasArgs().withDescription(
        "A local directory used for holding data for future runs (e.g.: /path/to/data/storage/location)").create());

    if (args.length == 0) {
      printHelpExit(options);
    }

    int port = DEFAULT_PORT;
    Set<URL> urls = new HashSet<>();
    File localStorage;

    try {
      CommandLine line = parser.parse(options, args);
      if (line.hasOption("url")) {
        for (String urlText : line.getOptionValues("url")) {
          urls.add(new URL(urlText));
        }
      } else {
        throw new RuntimeException("You forgot to specify a remote data URL");
      }

      if (line.hasOption("local-storage")) {
        localStorage = new File(line.getOptionValue("local-storage"));
      } else {
        throw new RuntimeException("You forgot to specify a local storage directory");
      }

    } catch (Exception e) {
      e.printStackTrace();
      printHelpExit(options);
      throw new RuntimeException("Failed!", e);
    }

    if (!localStorage.exists()) {
      localStorage.mkdirs();
    }

    if (!localStorage.exists() || !localStorage.isDirectory()) {
      System.out.println("Path given for local storage is either non-existent, or not a directory: " + localStorage.getAbsolutePath());
      System.exit(1);
    }

    if (!localStorage.canWrite()) {
      System.out.println("Directory is not writable: " + localStorage.getAbsolutePath());
      System.exit(1);
    }


    NcbiFactory ncbiFactory = new NcbiFactory();
    NcbiRestServer.startRestServer(port, ncbiFactory.make(localStorage, urls));

    logger.info(String.format("REST service started on port %s", port));
  }
}
