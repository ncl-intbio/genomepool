/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.ico2s.genomepool;

import org.ico2s.genomepool.assemblydb.AssemblyDb;
import org.ico2s.genomepool.assemblydb.AssemblyDbRamImpl;
import org.ico2s.genomepool.providers.assembly_provider.*;

import java.io.File;
import java.net.URL;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Keith Flanagan
 */
public class NcbiFactory {
  public Ncbi make(File localCache, URL remoteDataRoot) throws AssemblyListProviderException {
    return make(localCache, new UrlAssemblyListProvider(localCache, remoteDataRoot));
  }

  public Ncbi make(File localCache, Set<URL> remoteDataRoots) throws AssemblyListProviderException {
    Set<AssemblyListProvider> providers = remoteDataRoots.stream()
        .map(url -> new UrlAssemblyListProvider(localCache, url))
        .collect(Collectors.toSet());
    return make(localCache, new MultiSourceAssemblyListProvider(providers));
  }

  public Ncbi make(File localCache, AssemblyListProvider provider) throws AssemblyListProviderException {

    AssemblyListProvider filteredByLatestAndComplete
        = new LatestAssemblyListProvider(new CompleteGenomeAssemblyListProvider(provider));

    AssemblyDb assemblyDb = new AssemblyDbRamImpl();
    assemblyDb.addAll(filteredByLatestAndComplete.get());

    return new NcbiImpl(assemblyDb, filteredByLatestAndComplete, localCache);
  }
}
