/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.providers.assembly_provider_dir;

import org.ico2s.genomepool.parsers.genbank.data.GbkHeader;
import org.ico2s.genomepool.parsers.assembly_summary.AssemblySummary;

import java.util.Date;

/**
 * @author Keith Flanagan
 */
public class AssemblySummaryGeneratorImpl implements AssemblySummaryGenerator {
  private final String baseFtpPath;

  public AssemblySummaryGeneratorImpl(String baseFtpPath) {
    this.baseFtpPath = baseFtpPath;
  }

  @Override
  public AssemblySummary createSummary(String entryDirName, GbkHeader gbkHeader) {
    String ftpPath = baseFtpPath + "/" + entryDirName;


    AssemblySummary summary = new AssemblySummary();
    summary.setAsmName(gbkHeader.getVersion());            //
    summary.setAssemblyAccession(gbkHeader.getVersion());  // Hack to generate an ad-hock summary
    summary.setAssemblyDirectoryName(entryDirName);
    summary.setAssemblyLevel(AssemblySummary.AssemblyLevel.COMPLETE_GENOME);
    summary.setBioProjectId(gbkHeader.getVersion());
    summary.setBioSampleId(gbkHeader.getVersion());
    summary.setFtpPath(ftpPath);
    summary.setGbrsPairedAsm(gbkHeader.getVersion());
    summary.setGenomeRep(AssemblySummary.GenomeRepresentation.FULL);
    summary.setInfraSpecificName(gbkHeader.getSubSpeciesName());
    summary.setIsolate("");
    summary.setOrganismName(gbkHeader.getOrganismName());
    summary.setPairedAsmComp(AssemblySummary.PairedAsmComp.NA);
    summary.setRefseqCategory(AssemblySummary.RefseqCategory.NA);  //AssemblySummary.RefseqCategory.REPRESENTATIVE_GENOME
    summary.setReleaseType(AssemblySummary.ReleaseType.MAJOR);
    summary.setSeqRelDate(new Date());
    summary.setSpeciesTaxId(gbkHeader.getTaxonId());
    summary.setSubmitter("");
    summary.setTaxId(gbkHeader.getTaxonId());
    summary.setVersionStatus(AssemblySummary.VersionStatus.LATEST);
    summary.setWgsMaster("");

    return summary;
  }
}
