/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.providers.fasta_provider;

import org.ico2s.genomepool.assemblydb.AssemblyDb;
import org.ico2s.genomepool.assemblydb.AssemblyDbRamImpl;
import org.ico2s.genomepool.parsers.assembly_summary.AssemblySummary;
import org.ico2s.genomepool.parsers.fasta.FastaEntry;
import org.ico2s.genomepool.parsers.fasta.FastaParser;
import org.ico2s.genomepool.providers.assembly_provider.*;
import org.ico2s.util.filecache.FileCache;
import org.ico2s.util.filecache.FileCacheException;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * An implementation of <code>FastaProvider</code> that downloads FASTA files on-demand, and keeps them
 * cached locally for future (re)use.
 *
 * A method is provided to querySequenceOccurrences FASTA sequences by the parent assembly accession number. An instance of
 * <code>AssemblyDb</code> is used to resolve an <code>AssemblySummary</code> entry for the specified accession ID.
 *
 * Both DNA and protein FASTA sequences are supported by this provider. The type of sequence to be provided
 * must be specified in the constructor.
 *
 * @author Keith Flanagan
 */
public class CachedRemoteFastaProvider implements FastaProvider {
  private static final Logger logger = Logger.getLogger(CachedRemoteFastaProvider.class.getName());

  public enum FastaType {
    PROTEIN ("_protein.faa.gz"),
    DNA ("_genomic.fna.gz");

    private String fileSuffix;

    FastaType(String fileSuffix) {
      this.fileSuffix = fileSuffix;
    }

    public String getFileSuffix() {
      return fileSuffix;
    }
  }

  private final AssemblyDb assemblyDb;
  private final FileCache fileCache;
  private final FastaType fastaType;


  public CachedRemoteFastaProvider(AssemblyDb assemblyDb, File cacheDirRoot, FastaType fastaType) {
    this.assemblyDb = assemblyDb;
    this.fileCache = new FileCache(cacheDirRoot);
    this.fastaType = fastaType;
  }

  private URL makeUrl(AssemblySummary assemblySummary) throws FastaProviderException {
    if (assemblySummary == null) {
      throw new FastaProviderException("No assembly summary was specified!");
    }
    try {
      /*
       * The AssemblySummary only provides an FTP directory. We need to create the filename from other metadata.
       */
      StringBuilder urlText = new StringBuilder(256);
      urlText.append(assemblySummary.getFtpPath());
      urlText.append("/");
      // Take last directory name as the filename stem
      urlText.append(assemblySummary.getAssemblyDirectoryName())
          .append(fastaType.getFileSuffix());

      return new URL(urlText.toString());
    } catch (MalformedURLException e) {
      throw new FastaProviderException("Failed to construct a valid URL from: "+assemblySummary.getFtpPath(), e);
    }
  }

  private AssemblySummary lookupAssemblyBy(String assemblyAccession) throws FastaProviderException {
    try {
      return assemblyDb.getByAccession(assemblyAccession);
    } catch (Exception e) {
      throw new FastaProviderException("Failed to find assembly accession: "+assemblyAccession, e);
    }
  }

  @Override
  public InputStream getFasta(String assemblyAccession) throws FastaProviderException {
    try {
      AssemblySummary assemblySummary = lookupAssemblyBy(assemblyAccession);
      if (assemblySummary == null) {
        throw new FastaProviderException("Failed to find AssemblySummary for accession: "+assemblyAccession);
      }

      return fileCache.getAndOpen(makeUrl(assemblySummary));
    } catch (FileCacheException e) {
      throw new FastaProviderException("Failed to download / open file", e);
    }
  }

  @Override
  public List<FastaEntry> getParsed(String assemblyAccession) throws FastaProviderException {
    AssemblySummary assemblySummary = lookupAssemblyBy(assemblyAccession);
    if (assemblySummary == null) {
      throw new FastaProviderException("Failed to find AssemblySummary for accession: "+assemblyAccession);
    }

    try (InputStream is = fileCache.getAndOpen(makeUrl(assemblySummary))) {
      FastaParser parser = new FastaParser();
      return parser.parse(is);
    } catch (MalformedURLException e) {
      throw new FastaProviderException("Failed to construct a valid URL from: "+assemblySummary.getFtpPath(), e);
    } catch (FileCacheException e) {
      throw new FastaProviderException("Failed to download / open file", e);
    } catch (EOFException e) {
      fileCache.deleteLocalCopyOf(makeUrl(assemblySummary));
      throw new FastaProviderException("Failed to parse file due to EOFException. " +
          "Deleted the file (in case of a corrupt download). You should try the operation again.", e);
    } catch (IOException e) {
      throw new FastaProviderException("Failed to parse file", e);
    }
  }

  private static String streamToString(InputStream is) throws IOException {
    byte[] buffer = new byte[4096];
    StringBuilder text = new StringBuilder();
    for (int read = is.read(buffer); read != -1; read = is.read(buffer)) {
      text.append(new String(buffer, 0, read));
    }
    return text.toString();
  }
}
