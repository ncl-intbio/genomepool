/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.providers;

import org.ico2s.genomepool.assemblydb.AssemblyDb;
import org.ico2s.genomepool.assemblydb.AssemblyDbRamImpl;
import org.ico2s.genomepool.providers.assembly_provider.*;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Keith Flanagan
 */
public class AssemblyDbFromUrlsFactory {
  public static AssemblyListProvider makeAssemblyListProvider(File cacheRoot, URL url) {
    UrlAssemblyListProvider rawProvider = new UrlAssemblyListProvider(cacheRoot, url);
    LatestAssemblyListProvider latestProvider = new LatestAssemblyListProvider(rawProvider);
    CompleteGenomeAssemblyListProvider assemblyProvider = new CompleteGenomeAssemblyListProvider(latestProvider);
    return assemblyProvider;
  }

  public static AssemblyListProvider makeAssemblyListProvider(File localFileCache, Collection<String> urls)
      throws MalformedURLException {
    Set<AssemblyListProvider> providers = new HashSet<>();
    for (String url : urls) {
      UrlAssemblyListProvider rawProvider = new UrlAssemblyListProvider(localFileCache, new URL(url));
      LatestAssemblyListProvider latestProvider = new LatestAssemblyListProvider(rawProvider);
      CompleteGenomeAssemblyListProvider assemblyProvider = new CompleteGenomeAssemblyListProvider(latestProvider);

      providers.add(assemblyProvider);
    }

    return new MultiSourceAssemblyListProvider(providers);
  }

  public static AssemblyDb buildAssemblyDb(File localFileCache, Collection<String> urls)
      throws AssemblyListProviderException {
    List<AssemblyListProvider> assemblyListProviders = urls.stream().map(url -> {
      try {
        return makeAssemblyListProvider(localFileCache, new URL(url));
      } catch (MalformedURLException e) {
        throw new RuntimeException("Failed to create URL from: "+url, e);
      }
    }).collect(Collectors.toList());

    AssemblyDb assemblyDb = new AssemblyDbRamImpl();
    for (AssemblyListProvider provider : assemblyListProviders) {
      assemblyDb.addAll(provider.get());
    }

    return assemblyDb;
  }

}
