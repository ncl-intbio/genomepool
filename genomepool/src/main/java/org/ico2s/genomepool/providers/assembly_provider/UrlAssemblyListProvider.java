/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.providers.assembly_provider;

import org.ico2s.genomepool.parsers.assembly_summary.AssemblySummary;
import org.ico2s.genomepool.parsers.assembly_summary.AssemblySummaryParser;
import org.ico2s.util.filecache.FileCache;

import java.io.File;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * An AssemblyListProvider implementation that obtains its list of assemblies from an 'assembly_summary.txt' file
 * at the end of a URL.
 *
 * @author Keith Flanagan
 */
public class UrlAssemblyListProvider implements AssemblyListProvider {
  private static final Logger logger = Logger.getLogger(UrlAssemblyListProvider.class.getName());

  public static final String DEFAULT_ASSEMBLIES_FILE = "assembly_summary.txt";

  private final FileCache fileCache;
  private final String assembliesFilename = DEFAULT_ASSEMBLIES_FILE;

  private final URL assembliesRemoteFile;

  public UrlAssemblyListProvider(File cacheDirRoot, URL remoteScanDir) throws IllegalArgumentException {
    this.fileCache = new FileCache(cacheDirRoot);
    try {
      this.assembliesRemoteFile = new URL(remoteScanDir.toString() + "/" + assembliesFilename);
    } catch (MalformedURLException e) {
      throw new IllegalArgumentException("Could not create a new URL from: "+remoteScanDir.toString());
    }
  }

  @Override
  public Set<AssemblySummary> get() throws AssemblyListProviderException {
    try (InputStream fis = fileCache.getAndOpen(assembliesRemoteFile)) {
      AssemblySummaryParser parser = new AssemblySummaryParser();
      return parser.parse(fis);
    } catch (Exception e) {
      throw new AssemblyListProviderException("Failed to parse file content from: "+assembliesRemoteFile.toString(), e);
    }
  }


  public static void main(String[] args) throws MalformedURLException, AssemblyListProviderException {
    UrlAssemblyListProvider provider = new UrlAssemblyListProvider(
        new File("./temp/ncbi-cache"),
        new URL("ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/Bacillus_subtilis"));

    //provider.get();
    provider.get().forEach(v -> System.out.println(v.getAssemblyAccession()+", "+v.getRefseqCategory()));
  }
}
