/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.providers.assembly_provider;

import org.ico2s.genomepool.parsers.assembly_summary.AssemblySummary;

import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * An AssemblyListProvider implementation that combines several <code>AssemblyListProvider</code>
 * instances to provide a large list of assemblies, possibly sourced from different locations.
 *
 * Note: currently no duplicate checking is performed.
 *
 * @author Keith Flanagan
 */
public class MultiSourceAssemblyListProvider implements AssemblyListProvider {
  private static final Logger logger = Logger.getLogger(MultiSourceAssemblyListProvider.class.getName());

  private final Set<AssemblyListProvider> delegates;


  public MultiSourceAssemblyListProvider(Set<AssemblyListProvider> delegates) throws IllegalArgumentException {
    this.delegates = delegates;
  }

  @Override
  public Set<AssemblySummary> get() throws AssemblyListProviderException {
    if (delegates.isEmpty()) {
      throw new AssemblyListProviderException("No providers were specified!");
    }

    Set<AssemblySummary> assemblies = null;
    for (AssemblyListProvider provider : delegates) {
      if (assemblies == null) {
        assemblies = provider.get();
      } else {
        //assemblies = Stream.concat(assemblies, provider.get());
        assemblies.addAll(provider.get());
      }
    }
    return assemblies;
  }

}
