/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.providers.assembly_provider_dir;

import org.ico2s.genomepool.providers.assembly_provider.AssemblyListProvider;
import org.ico2s.genomepool.providers.assembly_provider.AssemblyListProviderException;
import org.ico2s.genomepool.parsers.assembly_summary.AssemblySummary;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * @author Keith Flanagan
 */
public class AssemblyListProviderDirImpl implements AssemblyListProvider {
  private static final Logger logger = Logger.getLogger(AssemblyListProviderDirImpl.class.getName());

  private final File inputDir;
  private final File outputDir;
  private final String baseUrl;
  private final Set<AssemblySummary> cachedSummaries;

  public AssemblyListProviderDirImpl(File srcGbkDirectory, String baseUrl) {
    this.inputDir = srcGbkDirectory;
    this.outputDir = new File(srcGbkDirectory, "out");
    this.baseUrl = baseUrl;
    this.cachedSummaries = new HashSet<>();
  }

  @Override
  public Set<AssemblySummary> get() throws AssemblyListProviderException {
    /*
     *
     * We're interested in a directory of '*.gbk' files, which will be converted into:
     * ./foo/foo_genomic.gbff.gz
     * ./foo/foo_protein.faa.gz
     * ./foo/foo_genomic.fna.gz
     */
    // TODO implement assembly summary caching in a file so that we're not expensively parsing GBK each time?

    try {
      if (cachedSummaries.isEmpty()) {
        AssemblySummaryBuilder builder = new AssemblySummaryBuilder();
        cachedSummaries.addAll(builder.build(inputDir, outputDir, baseUrl));
      }
      return cachedSummaries;
    } catch (Exception e) {
      throw new AssemblyListProviderException("Failed to build a new assembly summary dataset for files in: "+inputDir.getAbsolutePath());
    }
  }
}
