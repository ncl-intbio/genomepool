/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.providers.assembly_provider_dir;

import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.ico2s.genomepool.parsers.genbank.GbkParser;
import org.ico2s.genomepool.parsers.genbank.ParserException;
import org.ico2s.genomepool.parsers.genbank.data.Feature;
import org.ico2s.genomepool.parsers.genbank.data.GbkEntry;
import org.ico2s.genomepool.providers.assembly_provider.AssemblyListProviderException;
import org.ico2s.genomepool.parsers.assembly_summary.AssemblySummary;
import org.ico2s.genomepool.parsers.assembly_summary.AssemblySummaryParser;
import org.ico2s.genomepool.providers.assembly_provider.UrlAssemblyListProvider;
import org.ico2s.genomepool.providers.fasta_provider.CachedRemoteFastaProvider;
import org.ico2s.genomepool.providers.gbk_provider.CachedRemoteGbkEntryProvider;

import java.io.*;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import static org.apache.commons.cli.OptionBuilder.withLongOpt;

/**
 * @author Keith Flanagan
 */
public class AssemblySummaryBuilder {
  private static final Logger logger = Logger.getLogger(AssemblyListProviderDirImpl.class.getName());

  private static void printHelpExit(Options options) throws IOException {
    HelpFormatter formatter = new HelpFormatter();
    String cmdSyntax = "ad-hoc-assembly-summary-builder.sh";
    String header = "";
    String footer = "";
    int width = 80;
    formatter.printHelp(width, cmdSyntax, header, options, footer);

    System.exit(0);
  }

  public static void main(String[] args) throws IOException, AssemblyListProviderException {

    CommandLineParser cmdParser = new PosixParser();

    Options options = new Options();

    options.addOption(withLongOpt("base-dir").hasArgs().withDescription("The location of the GBK files to generate assembly_summary.txt and other support files for").create());
    options.addOption(withLongOpt("base-url").hasArgs().withDescription("The base URL at which the generated files will be hosted at").create());

    if (args.length == 0) {
      printHelpExit(options);
    }

    File baseDir;
    String baseUrl;

    try {
      CommandLine line = cmdParser.parse(options, args);

      if (line.hasOption("base-dir")) {
        baseDir = new File(line.getOptionValue("base-dir"));
      } else {
        throw new RuntimeException("A base directory is required");
      }
      if (line.hasOption("base-url")) {
        baseUrl = line.getOptionValue("base-url");
      } else {
        throw new RuntimeException("A base URL is required");
      }

    } catch (Exception e) {
      e.printStackTrace();
      printHelpExit(options);
      throw new RuntimeException("Failed", e);
    }

    AssemblySummaryBuilder builder = new AssemblySummaryBuilder();
    builder.build(baseDir, new File(baseDir, "out"), baseUrl);
  }

  public Set<AssemblySummary> build(File inputDir, File outputDir, String baseUrl) throws AssemblyListProviderException {
    /*
     *
     * We're interested in a directory of '*.gbk' files, which will be converted into:
     * ./foo/foo_genomic.gbff.gz
     * ./foo/foo_protein.faa.gz
     * ./foo/foo_genomic.fna.gz
     */
    File assemblySummaryFile = new File(outputDir, UrlAssemblyListProvider.DEFAULT_ASSEMBLIES_FILE);

    try {
      return buildAssemblySummaryData(inputDir, outputDir, baseUrl, assemblySummaryFile);
    } catch (Exception e) {
      throw new AssemblyListProviderException("Failed to build a new assembly summary dataset for files in: "+inputDir.getAbsolutePath(), e);
    }
  }

  private Set<AssemblySummary> buildAssemblySummaryData(File inputDir, File outputDir, String baseUrl, File assemblySummaryFile) throws IOException, ParserException {
    //File[] gbkFiles = baseDir.listFiles(pathname -> pathname.isFile() && pathname.getName().endsWith(".gbk.gz"));
    File[] gbkFiles = inputDir.listFiles(pathname -> pathname.isFile() && pathname.getName().endsWith(".genbank.gz"));
    //name.endsWith(CachedRemoteGbkEntryProvider.DEFAULT_GBFF_SUFFIX));
    if (gbkFiles == null) {
      throw new IOException("Failed to list directory: "+ inputDir.getAbsolutePath());
    }

    Set<AssemblySummary> summaries = new HashSet<>();
    for (File gbkFile : gbkFiles) {
      logger.info("Parsing: "+gbkFile.getAbsolutePath());
      File entryDir = new File(outputDir, gbkFile.getName().substring(0, gbkFile.getName().indexOf(".")));
      entryDir.mkdir();

      File genomicGbff = new File(entryDir, entryDir.getName()+ CachedRemoteGbkEntryProvider.DEFAULT_GBFF_SUFFIX);
      File genomicFna = new File(entryDir, entryDir.getName()+ CachedRemoteFastaProvider.FastaType.DNA.getFileSuffix());
      File proteinFaa = new File(entryDir, entryDir.getName()+CachedRemoteFastaProvider.FastaType.PROTEIN.getFileSuffix());

      copyFile(gbkFile, genomicGbff);
      GbkEntry gbkEntry = parseGenbank(genomicGbff);
      writeDnaFasta(gbkEntry, genomicFna);
      writeProteinFasta(gbkEntry, proteinFaa);

      AssemblySummaryGenerator summaryGenerator = new AssemblySummaryGeneratorImpl(baseUrl);
      summaries.add(summaryGenerator.createSummary(genomicGbff.getParentFile().getName(), gbkEntry.getHeader()));
    }

    writeSummaries(summaries, assemblySummaryFile);
    return summaries;
  }

  private void writeSummaries(Set<AssemblySummary> summaries, File destination) throws IOException {
    // TODO implement a file writer for AssemblySummary --> assembly_summary.txt
    String del = "\t";
    try (BufferedWriter bw = new BufferedWriter(new FileWriter(destination))) {
      bw.write("# Some header line should be here ...\n");
      for (AssemblySummary s : summaries) {
        StringBuilder line = new StringBuilder(1024);
        line.append(s.getAssemblyAccession()).append(del);
        line.append(s.getBioProjectId()).append(del);
        line.append(s.getBioSampleId()).append(del);
        line.append(s.getWgsMaster()).append(del);
        line.append(s.getRefseqCategory().getNcbiName()).append(del);
        line.append(s.getTaxId()).append(del);
        line.append(s.getSpeciesTaxId()).append(del);
        line.append(s.getOrganismName()).append(del);
        line.append(s.getInfraSpecificName()).append(del);
        line.append(s.getIsolate()).append(del);
        line.append(s.getVersionStatus().getNcbiName()).append(del);
        line.append(s.getAssemblyLevel().getNcbiName()).append(del);
        line.append(s.getReleaseType().getNcbiName()).append(del);
        line.append(s.getGenomeRep().getNcbiName()).append(del);
        line.append(AssemblySummaryParser.dateParser.format(s.getSeqRelDate())).append(del);
        line.append(s.getAsmName()).append(del);
        line.append(s.getSubmitter()).append(del);
        line.append(s.getGbrsPairedAsm()).append(del);
        line.append(s.getPairedAsmComp().getNcbiName()).append(del);
        line.append(s.getFtpPath()).append(del);

        line.append("\n");
        bw.write(line.toString());
      }
    }
  }

  private GbkEntry parseGenbank(File gbkFile) throws ParserException, IOException {
    GbkParser parser = new GbkParser();
    try (BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(gbkFile))))) {
        return parser.parseEntry(br);
      }
  }

  private void copyFile(File source, File destination) throws IOException {
    FileUtils.copyFile(source, destination);
  }

  private void writeDnaFasta(GbkEntry entry, File destination) throws IOException {
    try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(destination))))) {
      bw.write(">");
      bw.write(entry.getHeader().getVersion());
      bw.write("\n");
      // FIXME split into lines ...
      bw.write(entry.getDna());
      bw.write("\n");
    }
  }

  private void writeProteinFasta(GbkEntry entry, File destination) throws IOException {
    try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(destination))))) {
      for (Feature feature : entry.getFeatures()) {
        if (feature instanceof Feature.CDS) {
          Feature.CDS cds = (Feature.CDS) feature;
          StringBuilder sb = new StringBuilder(2048);
          sb.append(">").append(cds.getProteinId())
              .append(" ").append(cds.getProduct()).append("\n");
          // FIXME split into lines ...
          sb.append(cds.getTranslation()).append("\n");
          bw.write(sb.toString());
        }
      }
    }
  }
}
