/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.providers.assembly_provider_dir;

import org.ico2s.genomepool.parsers.genbank.data.GbkHeader;
import org.ico2s.genomepool.parsers.assembly_summary.AssemblySummary;

/**
 * Creates an AssemblySummary instance from a Genbank header.
 * Note that this is a hack for using your own directories of Genbank files along with data from NCBI.
 * This will actually use the ACCESSION.VERSION field in place of the assembly accession ID.
 *
 * @author Keith Flanagan
 */
public interface AssemblySummaryGenerator {
  AssemblySummary createSummary(String entryDirName, GbkHeader gbkHeader);
}
