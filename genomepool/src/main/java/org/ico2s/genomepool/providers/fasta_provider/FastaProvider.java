/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.providers.fasta_provider;

import org.ico2s.genomepool.parsers.fasta.FastaEntry;

import java.io.InputStream;
import java.util.List;

/**
 * @author Keith Flanagan
 */
public interface FastaProvider {
  /**
   * Provides a FASTA-formatted stream of proteins from all fragments of the specified <code>assemblySummary</code>.
   *
   * @param assemblyAccession the assembly to provide a FASTA file for.
   * @return a data stream containing a FASTA-formatted file. This contains proteins from ALL fragments (chromosomes,
   * plasmids, contigs, etc) from the specified assembly.
   * @throws FastaProviderException
   */
  InputStream getFasta(String assemblyAccession) throws FastaProviderException;

  /**
   * Locates the FASTA entries for the specified <code>assemblySummary</code> and parses the entries into a stream of
   * objects.
   *
   * @param assemblyAccession
   * @return a parallel stream of parsed FASTA entries. Note that the ordering might differ from the original data file.
   *
   * @throws FastaProviderException
   */
  List<FastaEntry> getParsed(String assemblyAccession) throws FastaProviderException;
}
