/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.providers.assembly_provider;

import org.ico2s.genomepool.parsers.assembly_summary.AssemblySummary;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A provider that returns genome assemblies that are annotated as being "Complete Genome", and
 * where the intent of the sequencing project was to provide a full genome representation ("Full").
 *
 * See http://www.ncbi.nlm.nih.gov/assembly/help/
 *
 * @author Keith Flanagan
 */
public class CompleteGenomeAssemblyListProvider implements AssemblyListProvider {
  private static final Logger logger = Logger.getLogger(CompleteGenomeAssemblyListProvider.class.getName());

  private final AssemblyListProvider delegate;

  public CompleteGenomeAssemblyListProvider(AssemblyListProvider delegate) {
    this.delegate = delegate;
  }

  @Override
  public Set<AssemblySummary> get() throws AssemblyListProviderException {
    return delegate.get().stream().filter(v ->
        v.getGenomeRep() == AssemblySummary.GenomeRepresentation.FULL &&
        v.getAssemblyLevel() == AssemblySummary.AssemblyLevel.COMPLETE_GENOME)
        .collect(Collectors.toSet());
  }



  public static void main(String[] args) throws MalformedURLException, AssemblyListProviderException {
    UrlAssemblyListProvider rawProvider = new UrlAssemblyListProvider(
        new File("./temp/ncbi-cache"),
        new URL("ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/Bacillus_subtilis"));
    LatestAssemblyListProvider latestProvider = new LatestAssemblyListProvider(rawProvider);
    CompleteGenomeAssemblyListProvider provider = new CompleteGenomeAssemblyListProvider(latestProvider);
    //provider.get();
    provider.get().forEach(v -> System.out.println(v.getAssemblyAccession()+", "+v.getFtpPath()));
  }
}
