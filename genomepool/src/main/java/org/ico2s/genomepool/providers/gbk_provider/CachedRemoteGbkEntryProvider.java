/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.providers.gbk_provider;

import org.ico2s.genomepool.parsers.assembly_summary.AssemblySummary;
import org.ico2s.genomepool.parsers.genbank.GbkParser;
import org.ico2s.genomepool.parsers.genbank.ParserException;
import org.ico2s.genomepool.parsers.genbank.data.GbkEntry;
import org.ico2s.genomepool.providers.assembly_provider.*;
import org.ico2s.util.filecache.FileCache;
import org.ico2s.util.filecache.FileCacheException;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * @author Keith Flanagan
 */
public class CachedRemoteGbkEntryProvider implements GbkProvider {
  private static final Logger logger = Logger.getLogger(CachedRemoteGbkEntryProvider.class.getName());

  public static final String DEFAULT_GBFF_SUFFIX = "_genomic.gbff.gz";

  private final FileCache fileCache;
  private final String suffix;


  public CachedRemoteGbkEntryProvider(File cacheDirRoot) {
    this.fileCache = new FileCache(cacheDirRoot);
    this.suffix = DEFAULT_GBFF_SUFFIX;
  }

//  private URL makeUrl(AssemblySummary assemblySummary) throws GbkProviderException {
//    try {
//      /*
//       * The AssemblySummary only provides an FTP directory. We need to create the filename from other metadata.
//       */
//      StringBuilder urlText = new StringBuilder(128);
//      urlText.append(assemblySummary.getFtpPath());
//      urlText.append("/");
//      urlText.append(assemblySummary.getAssemblyAccession())
//          .append("_")
//          .append(assemblySummary.getAsmName())
//          .append(suffix);
//
//      /*
//       * Now need to run a search/replace since at least some assembly names have spaces within them.
//       * On the NCBI FTP site, names with spaces appear to have been replaced with underscores.
//       */
//      String finalUrlText = urlText.toString().replace(' ', '_');
//
//      return new URL(finalUrlText);
//    } catch (MalformedURLException e) {
//      throw new GbkProviderException("Failed to construct a valid URL from: "+assemblySummary.getFtpPath(), e);
//    }
//  }


  private URL makeUrl(AssemblySummary assemblySummary) throws GbkProviderException {
    try {
      /*
       * The AssemblySummary only provides an FTP directory. We need to create the filename from other metadata.
       */
      StringBuilder urlText = new StringBuilder(128);
      urlText.append(assemblySummary.getFtpPath());
      urlText.append("/");
      // Take last directory name as the filename stem
      urlText.append(assemblySummary.getAssemblyDirectoryName()).append(suffix);

      return new URL(urlText.toString());
    } catch (MalformedURLException e) {
      throw new GbkProviderException("Failed to construct a valid URL from: "+assemblySummary.getFtpPath(), e);
    }
  }

  @Override
  public InputStream getGbk(AssemblySummary assemblySummary) throws GbkProviderException {
    try {
      return fileCache.getAndOpen(makeUrl(assemblySummary));
    } catch (FileCacheException e) {
      throw new GbkProviderException("Failed to download / open file", e);
    }
  }

  @Override
  public Stream<GbkEntry> getParsed(AssemblySummary assemblySummary) throws GbkProviderException {
    return getParsedAsList(assemblySummary).parallelStream();
  }

  @Override
  public List<GbkEntry> getParsedAsList(AssemblySummary assemblySummary) throws GbkProviderException {
    try (BufferedReader br = new BufferedReader(new InputStreamReader(fileCache.getAndOpen(makeUrl(assemblySummary))))) {
      GbkParser parser = new GbkParser();
      GbkEntry entry;
      List<GbkEntry> fragments = new ArrayList<>();
      for (entry = parser.parseEntry(br); entry != null; entry = parser.parseEntry(br)) {
        // Some Genbank entries don't have their own metadata!
        if (entry.getHeader().getBioProjectId() == null) {
          entry.getHeader().setBioProjectId(assemblySummary.getBioProjectId());
        }
        if (entry.getHeader().getAssemblyId() == null) {
          entry.getHeader().setAssemblyId(assemblySummary.getAssemblyAccession());
        }
        if (entry.getHeader().getBioSampleId() == null) {
          entry.getHeader().setBioSampleId(assemblySummary.getBioSampleId());
        }
        if (entry.getHeader().getAssemblyName() == null) {
          entry.getHeader().setAssemblyName(assemblySummary.getAsmName());
        }

        fragments.add(entry);
      }

      return fragments;
    } catch (MalformedURLException e) {
      throw new GbkProviderException("Failed to construct a valid URL from: "+assemblySummary.getFtpPath(), e);
    } catch (FileCacheException e) {
      throw new GbkProviderException("Failed to download / open file", e);
    } catch (IOException | ParserException e) {
      throw new GbkProviderException("Failed to parse file", e);
    }
  }

  private static String streamToString(InputStream is) throws IOException {
    byte[] buffer = new byte[4096];
    StringBuilder text = new StringBuilder();
    for (int read = is.read(buffer); read != -1; read = is.read(buffer)) {
      text.append(new String(buffer, 0, read));
    }
    return text.toString();
  }

}
