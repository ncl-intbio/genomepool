/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.providers.assembly_provider;

import org.ico2s.genomepool.parsers.assembly_summary.AssemblySummary;

import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

/**
 * @author Keith Flanagan
 */
public interface AssemblyListProvider {
  /**
   * Obtains and parses a 'assembly_summary.txt' file available from the NCBI.
   *
   * @return returns a set of objects (each one represents a single assembly). Note that the ordering of the
   * objects may differ from that of the original data source since a parallel stream is returned.
   * @throws AssemblyListProviderException
   */
  Set<AssemblySummary> get() throws AssemblyListProviderException;
}
