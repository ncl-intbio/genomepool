/*
 * Copyright 2017 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.ico2s.genomepool;

import com.google.gson.reflect.TypeToken;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.ico2s.genomepool.parsers.assembly_summary.AssemblySummary;
import org.ico2s.genomepool.parsers.fasta.FastaEntry;
import org.ico2s.util.WsClientBase;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

/**
 * @author Keith Flanagan
 */
public class NcbiRestClient extends WsClientBase implements Ncbi {

  private static final Logger logger = Logger.getLogger(NcbiRestClient.class.getName());

  public NcbiRestClient(String baseUrl) {
    super(baseUrl);
  }

  @Override
  public AssemblySummary getAssembly(String accession) throws GenomePoolException {
    HttpGet req = new HttpGet(wsUrl + "/assembly/" + accession);
    try {
      HttpResponse res = client.execute(req);
      return consumeResponse(res, new TypeToken<AssemblySummary>(){});
    } catch (IOException e) {
      throw new GenomePoolException("Operation failed", e);
    }
  }

  @Override
  public long countAssemblies() throws GenomePoolException {
    HttpGet req = new HttpGet(wsUrl + "/countAssemblies");
    try {
      HttpResponse res = client.execute(req);
      return consumeResponse(res, new TypeToken<Long>(){});
    } catch (IOException e) {
      throw new GenomePoolException("Operation failed", e);
    }
  }

  @Override
  public Set<AssemblySummary> listAssemblies() throws GenomePoolException {
    HttpGet req = new HttpGet(wsUrl + "/assemblies");
    try {
      HttpResponse res = client.execute(req);
      return consumeResponse(res, new TypeToken<Set<AssemblySummary>>(){});
    } catch (IOException e) {
      throw new GenomePoolException("Operation failed", e);
    }
  }

  @Override
  public Set<AssemblySummary> textSearchAssemblies(String searchText) throws GenomePoolException {
    HttpPost req = new HttpPost(wsUrl + "/assembly-search");
    try {
      req.setHeader("Content-Type", "application/text");
      req.setEntity(new StringEntity(searchText));
      HttpResponse res = client.execute(req);
      return consumeResponse(res, new TypeToken<Set<AssemblySummary>>(){});
    } catch (IOException e) {
      throw new GenomePoolException("Operation failed", e);
    }
  }

  @Override
  public List<FastaEntry> getDnaSequences(String accession) throws GenomePoolException {
    HttpGet req = new HttpGet(wsUrl + "/dna/"+accession);
    try {
      HttpResponse res = client.execute(req);
      return consumeResponse(res, new TypeToken<List<FastaEntry>>(){});
    } catch (IOException e) {
      throw new GenomePoolException("Operation failed", e);
    }
  }

  @Override
  public List<FastaEntry> getProteinSequences(String accession) throws GenomePoolException {
    HttpGet req = new HttpGet(wsUrl + "/protein/"+accession);
    try {
      HttpResponse res = client.execute(req);
      return consumeResponse(res, new TypeToken<List<FastaEntry>>(){});
    } catch (IOException e) {
      throw new GenomePoolException("Operation failed", e);
    }
  }
}
