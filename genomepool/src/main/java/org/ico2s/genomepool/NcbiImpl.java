/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool;

import org.ico2s.genomepool.parsers.fasta.FastaEntry;
import org.ico2s.genomepool.assemblydb.AssemblyDb;
import org.ico2s.genomepool.providers.assembly_provider.AssemblyListProvider;
import org.ico2s.genomepool.parsers.assembly_summary.AssemblySummary;
import org.ico2s.genomepool.providers.fasta_provider.CachedRemoteFastaProvider;
import org.ico2s.genomepool.providers.fasta_provider.FastaProvider;

import java.io.File;
import java.util.List;
import java.util.Set;

/**
 * @author Keith Flanagan
 */
public class NcbiImpl implements Ncbi {

  private final AssemblyDb assemblyDb;
  private final AssemblyListProvider assemblyListProvider;
  private final FastaProvider dnaFastaProvider;
  private final FastaProvider proteinFastaProvider;

  public NcbiImpl(AssemblyDb assemblyDb, AssemblyListProvider assemblyListProvider, File cacheDirRoot) {
    this.assemblyDb = assemblyDb;
    this.assemblyListProvider = assemblyListProvider;
    this.dnaFastaProvider = new CachedRemoteFastaProvider(assemblyDb, cacheDirRoot, CachedRemoteFastaProvider.FastaType.DNA);
    this.proteinFastaProvider = new CachedRemoteFastaProvider(assemblyDb, cacheDirRoot, CachedRemoteFastaProvider.FastaType.PROTEIN);
  }

//  @Override
//  public void store(AssemblySummary assemblySummary) throws GenomePoolException {
//    assemblyDb.add(assemblySummary);
//  }

  @Override
  public AssemblySummary getAssembly(String accession) throws GenomePoolException {
    try {
      return assemblyDb.getByAccession(accession);
    } catch (Exception e) {
      e.printStackTrace();
      throw new GenomePoolException("Failed to perform operation", e);
    }
  }

  @Override
  public long countAssemblies() throws GenomePoolException {
    try {
      return assemblyListProvider.get().size();
    } catch (Exception e) {
      e.printStackTrace();
      throw new GenomePoolException("Failed to perform operation", e);
    }
  }

  @Override
  public Set<AssemblySummary> listAssemblies() throws GenomePoolException {
    try {
      return assemblyListProvider.get();
    } catch (Exception e) {
      e.printStackTrace();
      throw new GenomePoolException("Failed to perform operation", e);
    }
  }

  @Override
  public Set<AssemblySummary> textSearchAssemblies(String searchText) throws GenomePoolException {
    try {
      return assemblyDb.textSearch(searchText);
    } catch (Exception e) {
      e.printStackTrace();
      throw new GenomePoolException("Failed to perform operation", e);
    }
  }

  @Override
  public List<FastaEntry> getDnaSequences(String accession) throws GenomePoolException {
    try {
      return dnaFastaProvider.getParsed(accession);
    } catch (Exception e) {
      e.printStackTrace();
      throw new GenomePoolException("Failed to perform operation", e);
    }
  }

  @Override
  public List<FastaEntry> getProteinSequences(String accession) throws GenomePoolException {
    try {
      return proteinFastaProvider.getParsed(accession);
    } catch (Exception e) {
      e.printStackTrace();
      throw new GenomePoolException("Failed to perform operation", e);
    }
  }
}
