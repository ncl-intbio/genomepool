/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool;

import org.ico2s.genomepool.parsers.fasta.FastaEntry;
import org.ico2s.genomepool.parsers.assembly_summary.AssemblySummary;

import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

/**
 * @author Keith Flanagan
 */
public interface Ncbi {
//  void store(AssemblySummary assemblySummary) throws GenomePoolException;
  AssemblySummary getAssembly(String accession) throws GenomePoolException;
  long countAssemblies() throws GenomePoolException;
  Set<AssemblySummary> listAssemblies() throws GenomePoolException;
  Set<AssemblySummary> textSearchAssemblies(String searchText) throws GenomePoolException;


  List<FastaEntry> getDnaSequences(String accession) throws GenomePoolException;
  List<FastaEntry> getProteinSequences(String accession) throws GenomePoolException;
}
