/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.genomepool.assemblydb;

import org.ico2s.genomepool.parsers.assembly_summary.AssemblySummary;

import java.util.*;

/**
 * @author Keith Flanagan
 */
public class AssemblyDbRamImpl implements AssemblyDb {
  private final Map<String, AssemblySummary> byAccession;

  public AssemblyDbRamImpl() {
    this.byAccession = new HashMap<>();
  }

  public AssemblyDbRamImpl(Collection<AssemblySummary> assemblies) {
    this();
    for (AssemblySummary assemblySummary : assemblies) {
      add(assemblySummary);
    }
  }

  @Override
  public void add(AssemblySummary assemblySummary) {
    byAccession.put(assemblySummary.getAssemblyAccession(), assemblySummary);
  }

  @Override
  public void addAll(Collection<AssemblySummary> assemblies) {
    for (AssemblySummary assemblySummary : assemblies) {
      byAccession.put(assemblySummary.getAssemblyAccession(), assemblySummary);
    }
  }

  @Override
  public AssemblySummary getByAccession(String accession) throws AssemblyDbException {
    return byAccession.get(accession);
  }

  @Override
  public Set<AssemblySummary> getAll() throws AssemblyDbException {
    return new HashSet<>(byAccession.values());
  }

  @Override
  public Set<AssemblySummary> textSearch(String search) throws AssemblyDbException {
    Set<AssemblySummary> matching = new HashSet<>();

    for (AssemblySummary assembly : byAccession.values()) {
      if (assembly.getOrganismName().contains(search)) {
        matching.add(assembly);
      }
      else if (assembly.getAssemblyAccession().contains(search)) {
        matching.add(assembly);
      }
      else if (assembly.getBioProjectId().contains(search)) {
        matching.add(assembly);
      }
      else if (assembly.getBioSampleId().contains(search)) {
        matching.add(assembly);
      }
    }

    return matching;
  }
}
