package org.ico2s.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.microbasecloud.halogen.store.HalogenStorageException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @author James McLaughlin
 * @author Keith Flanagan
 */
public class WsClientBase {

    protected PoolingHttpClientConnectionManager connectionManager;
    protected HttpClient client;
    protected String wsUrl;
    protected Gson gson;

    public WsClientBase(String wsUrl) {

        this.wsUrl = wsUrl;

        gson = GsonFactory.make();

        connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(500);
        // Increase default max connection per route to 500
        connectionManager.setDefaultMaxPerRoute(500);
        client = HttpClients.custom().setConnectionManager(connectionManager).build();

    }

    protected String encodeURIComponent(String str) throws HalogenStorageException {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new HalogenStorageException(e);
        }
    }


    protected void checkResponseCode(HttpResponse response) throws HalogenStorageException
    {
        int statusCode = response.getStatusLine().getStatusCode();

        if(statusCode >= 300)
        {
            throw new HalogenStorageException("HTTP " + statusCode);
        }
    }

    protected <T> T objectFromResponse(HttpResponse res, TypeToken<T> typeToken) {
        String jsonText = null;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            res.getEntity().writeTo(baos);
            return gson.fromJson(new String(baos.toByteArray()), typeToken.getType());
        } catch (Exception e) {
            throw new RuntimeException("Failed to extract an object from a HTTP response. " +
                "Expected type was: " + typeToken+". JSON text was: "+jsonText, e);
        }
    }

    protected <T> T consumeResponse(HttpResponse res, TypeToken<T> typeToken) throws IOException {
        try {
            checkResponseCode(res);
            return objectFromResponse(res, typeToken);
        } finally {
            EntityUtils.consume(res.getEntity());
        }
    }

}
