/*
 * Copyright 2015 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.ico2s.util.filecache;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.Semaphore;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

/**
 * @author Keith Flanagan
 */
public class FileCache {
  private static final Logger logger = Logger.getLogger(FileCache.class.getName());

  private static final int DEFAULT_MAX_CONCURRENT_DLS = 5;
  private static final int DEFAULT_TIMEOUT_MS = 60000;

  private final Semaphore maxConcurrentDownloads;
  private final File cacheDirRoot;

  private final int timeout = DEFAULT_TIMEOUT_MS;

  public FileCache(Semaphore maxConcurrentDownloads, File cacheDirRoot) {
    this.maxConcurrentDownloads = maxConcurrentDownloads;
    this.cacheDirRoot = cacheDirRoot;
    logger.info("Created local filesystem cache. Configured with an available number of permits: "
        + maxConcurrentDownloads.availablePermits());
  }

  public FileCache(int maxConcurrentDownloads, File cacheDirRoot) {
    this(new Semaphore(maxConcurrentDownloads), cacheDirRoot);
  }

  public FileCache(File cacheDirRoot) {
    this(DEFAULT_MAX_CONCURRENT_DLS, cacheDirRoot);
  }

  public boolean existsLocally(URL remote) {
    File localFile = remoteToLocal(remote, false);
    return localFile.exists();
  }

  public InputStream getAndOpen(URL remote) throws FileCacheException {
    try {
      FileInputStream is = new FileInputStream(get(remote));
      if (remote.getFile().endsWith(".gz")) {
        return new GZIPInputStream(is);
      } else {
        return is;
      }
    } catch (Exception e) {
      throw new FileCacheException("Failed to open "+remote.toString(), e);
    }
  }

  public boolean deleteLocalCopyOf(URL remote) {
    File localFile = remoteToLocal(remote, false);
    logger.info(String.format("Going to delete local copy: %s of remote: %s", localFile.getAbsolutePath(), remote.toString()));
    return localFile.delete();
  }

  public File get(URL remote) throws FileCacheException {
    // Path to full file
    File localFile = remoteToLocal(remote, false);
    if (localFile.exists()) {
      return localFile;
    }

    // Path to directory that will contain the file
    File localDir = remoteToLocal(remote, true);
    if (!localDir.exists()) {
      boolean success = localDir.mkdirs();
      if (!success) {
        throw new FileCacheException("Failed to create local directory: "+localDir.getAbsolutePath());
      }
    }

    maxConcurrentDownloads.acquireUninterruptibly();
    try {
      logger.info("Downloading remote file: "+remote.toString()+" --> " + localFile.getAbsolutePath());
      File localTmpFile = new File(localFile.getParent(), localFile.getName()+".tmp_dl");
      FileUtils.copyURLToFile(remote, localTmpFile, timeout, timeout);
      // When the transfer has completed successfully, rename the file
      boolean renameResult = localTmpFile.renameTo(localFile);
      if (!renameResult) {
        throw new FileCacheException("Renaming temporary file failed: "+localTmpFile.getAbsolutePath() +
            " --> " + localFile.getAbsolutePath());
      }
      return localFile;
    } catch (IOException e) {
      throw new FileCacheException("Failed to download file: "+remote, e);
    } finally {
      maxConcurrentDownloads.release();
    }
  }


  /**
   * Given a remote URL of something like:
   * ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/Bacillus_subtilis
   *
   * Returns a local cache path, eg:
   * /Users/keith/Development/isense/isense/./temp/ncbi-cache/ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/Bacillus_subtilis
   *
   * @param remotePath
   * @return
   */
  private File remoteToLocal(URL remotePath, boolean snipLast) {
    String filePath = remotePath.getFile();

    if (snipLast && filePath.contains("/")) {
      filePath = filePath.substring(0, filePath.lastIndexOf('/'));
    }

    return new File(cacheDirRoot, remotePath.getHost()+File.separator+filePath);
  }

}
