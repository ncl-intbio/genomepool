#!/bin/bash

cd /genomepool

./NcbiRestServerLauncher.sh \
  --local-storage /mnt/data \
  --url ${URLS}

# A hack to keep the Docker image alive, since the above are forked
tail -f /dev/null

